Directory content: 
-----------------

- `var-tool` - source code of the tool creating a *.var* file 
- `phase-refinery` - the phaser source code
- `datafiles` - datafiles which user can use to test functionality of the algorithms
    - `chr20_panel.bcf` - reference panel
    - `chr20_genmap.txt` - genotype map
    - `chr20_selected_samples.bcf` - genotypes to phase
    - `HG00096/chr20_len_1000_err_0.01_alignment.bam` - aligned simulated read set for sample *HG00096* 
      (The read set property: has average coverage *15*, contains reads of length *1k* with *0.01%* seq. error rate, covers the first *1/4* of the *20th* chromosome)
    - `HG00096/chr20_len_1000_err_0.01_alignment.bai` - index to the `HG00096/chr20_len_1000_err_0.01_alignment.bam` file 
- `requirements.txt` - list of python packages required for *var-tool* project execution
- `text` - master's thesis text in *PDF* and its latex source code 

----------------------------------------------------------------------------------------------------------------------------------------
To phase the first sample (*HG00096*) from the `chr20_selected_samples.bcf` utilizing the given reference panel `chr20_panel.bcf` and reads from the given
 `HG00096/chr20_len_1000_err_0.01_alignment.bam` file, user is recommended to follow the steps:

- **Step 1: create .var file**

	To create *.var* file run: 

	```
	$ python3 var-tool/src/createVar.py datafiles/HG00096/chr20_len_1000_err_0.01_alignment.bam datafiles/chr20_selected_samples.bcf datafiles/HG00096/chr20_len_1000_err_0.01 HG00096 --indels True
	```
	
	The created `chr20_len_1000_err_0.01.var` file could be found in `datafiles/HG00096/`

- **Step 2: phase sample**

	To phase the first *475k* heterozygous positions of the *HG00096* sample file run: 
	
	```
	$ ./phase-refinery/target/phase --gmap datafiles/chr20_genmap.txt --input datafiles/chr20_selected_samples.bcf --panel datafiles/chr20_panel.bcf --output datafiles/HG00096/chr20_475000pos_phased --reads datafiles/HG00096/chr20_len_1000_err_0.01.var -M 475000 -D 100
	```
	
	This might take several minutes. The phased genotype is stored at `datafiles/HG00096/chr20_475000pos_phased-best.vcf.gz`

- **Step 3: estimate phasing accuracy**

	Since we posses true haplotype for the individual, we can now check phasing accuracy. 
	To estimate switch error for the conducted experiment, user can use *--gzdiff* options of the *vcftools* as follows: 
	
	```
	$ vcftools --bcf datafiles/chr20_selected_samples.bcf --gzdiff datafiles/HG00096/chr20_475000pos_phased-best.vcf.gz --diff-switch-error --out datafiles/HG00096/chr20_475000pos_phased-best --chr 20 --to-bp 16230000
	```
	
The result will be stored at `HG00096/chr20_475000pos_phased-best.diff.switch` (list of inconsistencies) and `HG00096/chr20_475000pos_phased-best.diff.indv.switch` (switch error for each sample).
The switch error for sample *HG00096* can be found in `HG00096/chr20_475000pos_phased-best.diff.indv.switch` and should be equal to *0.00968625 (~0.97%)*.
	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

For more details about the algorithms functionality see `text/hap_estimation_using_seq_reads.pdf`


