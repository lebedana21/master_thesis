#![feature(i128_type)]

extern crate env_logger;
extern crate stopwatch;
extern crate phasing;
extern crate pbwt;

use phasing::utils::{timeit, ChainBitVec};
use phasing::RefIndex;
use pbwt::BitVec;
//use phasing::genoalg::{State, RefSet, RefPanel, RefSetTrait};

struct IncHashK128<V> {
    data: Vec<(u128, V)>,
    size: usize,
}
const IHK128_MOD: u128 = 1231231564132326553;

impl<V: Default + Clone> IncHashK128<V> {
    pub fn with_capacity(cap: usize) -> Self {
        let mut d = Vec::new();
        d.resize(cap, (IHK128_MOD, V::default()));
        IncHashK128 { data: d, size: 0 }
    }
    fn hash(&self, k: u128, i: u128) -> usize {
        (k.wrapping_mul(i + 1) % IHK128_MOD % (self.data.len() as u128)) as usize
    }
    pub fn insert(&mut self, k: u128, v: V) {
        for i in 0..10 {
            let h = self.hash(k, i);
            if self.data[h].0 == k {
                panic!("Key already present");
            }
            if self.data[h].0 == IHK128_MOD {
                self.data[h].0 = k;
                self.data[h].1 = v;
                //std::mem::swap(&mut self.data[h].1, &mut v);
                return;
            }
        }
        panic!("Too many collisions in u128 hash table");
    }
    pub fn get(&self, k: u128) -> Option<&V> {
        for i in 0..10 {
            let h = self.hash(k, i);
            if self.data[h].0 == k {
                return Some(&self.data[h].1);
            }
            if self.data[h].0 == IHK128_MOD {
                return None;
            }
        }
        panic!("Too many collisions in u128 hash table");
    }
}


fn main() {
    env_logger::init().unwrap();
    timeit("Insert 1M bits into ChanBitVec", || {
        let mut cbv = ChainBitVec::new();
        for i in 0..1_000_000 {
            cbv.push(i % 2 == 0);
        }
    });

    timeit(
        "Insert 1M bits into ChanBitVec and store clones on the way",
        || {
            let mut vec = Vec::with_capacity(1_000_000);
            let mut cbv = ChainBitVec::new();
            for i in 0..1_000_000 {
                cbv.push(i % 2 == 0);
                vec.push(cbv.clone());
            }
        },
    );
}
