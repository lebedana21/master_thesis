#![allow(non_snake_case)]
//#![link_args = "-L /home/gavento/.local/lib"]

#[macro_use]
extern crate log;
extern crate fern;
extern crate phasing;
extern crate stopwatch;
extern crate chrono;
extern crate rayon;
extern crate clap;

#[allow(unused_imports)]
use rayon::prelude::*;
use stopwatch::Stopwatch;
use clap::App;
use formats::readpanel_fromfile;
use std::collections::HashMap;

#[allow(unused_imports)]
use phasing::*;

/// Parse a list of type "1,3-6,8,9,10-20" into Vec<usize>
/// Any parse errors lead to a `panic!`
fn parse_range_list(s: &str) -> Vec<Index> {
    let mut r = Vec::new();
    for ss in s.split(",") {
        if let Some(cpos) = ss.find("-") {
            let (fs, ts) = ss.split_at(cpos);
            for i in fs.parse().expect("parsing range start") ..
                (ts[1..].parse::<Index>().expect("parsing range end") + 1) {
                r.push(i);
            }
        } else {
            r.push(ss.parse().expect("parsing list element"));
        }
    }
    r
}

fn main() {

    // Unused arguments:
    // -S, --samples [S]     'Numbers of variation samples to resolve (e.g. 1-2,6,8,20-23) [default: all]' - currently 0 always
    // -s, --state-likelihood 'Uses state likelihood instead of candidate likelihood with -D, -P and output'
    // -a, --IBD [a]         'Expected IBD length in centiMorgans (cM) [default: 1.0]'

    let args = App::new("phase")
        .author("Tomas Gavenciak <gavento@ucw.cz> & Anastasia Lebedeva <lebedana@fel.cvut.cz>")
        .args_from_usage("
             -p, --panel <PANEL>   'Reference panel file name (VCF/BCF)'
             -i, --input <INPUT>   'Variation sample input file name (VCF/BCF)'
             -o, --output <OUTPUT> 'Output base name (a suffix is added)'
             -g, --gmap <GMAP>     'Genetic map file'
             -r, --reads [READS]   'Preprocessed reads filename (VAR)'
             -M, --markers [M]     'Limit on markers to read from the panel and samples [default:all]'
             -N, --refs [N]        'Limit on references read from the panel [default: all]'
             -D, --drop-states [D] 'States kept after dropping least-likely [default: 1000]'
             -d, --debug           'Debug logging mode'
        ").get_matches();

    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{:5}][{:16}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S%.3f]"),
                record.level(),
                record.target(),
                message
            ))
        })
        .level(if args.is_present("debug") {
            log::LevelFilter::Debug } else { log::LevelFilter::Info })
        .chain(::std::io::stderr())
        .apply()
        .unwrap();
    
    let a_M: Option<Index> = args.value_of("markers").map(|s| s.parse().expect("parsing M"));
    let a_N: Option<Index> = args.value_of("refs").map(|s| s.parse().expect("parsing N"));
    let a_prefix = args.value_of("output").unwrap();
    let a_input = args.value_of("input").unwrap();
    let a_gmap = args.value_of("gmap").unwrap();
    let a_panel = args.value_of("panel").unwrap();
    // let a_samples: Option<Vec<Index>> = args.value_of("samples").map(|s| parse_range_list(s));
    let a_samples: Option<Vec<Index>> = Some(parse_range_list("0"));
    // let a_state_l = args.is_present("state-likelyhood");
    let a_state_l = false;
    let a_D = args.value_of("drop-states").map(|s| s.parse().expect("parsing D")).unwrap_or(1000);
    let a_IBD: f64 = args.value_of("IBD").map(|s| s.parse().expect("parsing IBD")).unwrap_or(1.0);
    let a_reads = args.value_of("reads");

    let st1 = Stopwatch::start_new();
    let gm = GeneMap::read(&a_gmap).expect("reading gene map");
    info!("gene map {:?} read in {} ms, {} points", a_gmap, st1.elapsed_ms(), gm.locs.len());

    let st2 = Stopwatch::start_new();
    let input0 = SampleSet::read(&a_input, &a_samples, true).expect("reading input");
    info!("input samples {:?} read in {} ms, {} samples", a_input, st2.elapsed_ms(), input0.m());

    let st3 = Stopwatch::start_new();
    let refrange: Option<Vec<Index>> = a_N.map(|n| (0..n).collect());
    let panel0 = SampleSet::read(&a_panel, &refrange, false).expect("reading reference panel");
    info!("panel samples {:?} read in {} ms, {} haplotypes", a_panel, st3.elapsed_ms(), 2 * panel0.m());

    let st4 = Stopwatch::start_new();
    let read_panel = readpanel_fromfile(a_reads);
    if a_reads.is_some() {
        info!("reads {:?} read in {} ms, {} variants", &a_reads.unwrap(), st4.elapsed_ms(), read_panel.len());
    } else{
        info!("reads path was not specified");
    }

    let cfg = ComputationCfg {
        limit_states: a_D,
        new_var_prob: Likelyhood::from_float(1e-6),
        mean_ibd: a_IBD * 0.01,
        use_state_likelihood: a_state_l,
    };

    let mut c = Computation::new(panel0, input0, gm, cfg, read_panel, 0);
    let st = Stopwatch::start_new();
    let mut num: Index = 0;
    while c.step().unwrap() {
        num += 1;
        if a_M.is_some() && a_M.clone().unwrap() <= num {
            break;
        }
        if num % (999 / c.samples.len() as Index + 1) == 0 {
            let dt = st.elapsed_ms();
            debug!(".. ran {} steps in {} ms ({} steps/s)",
                num, dt, 1000.0 * (num as f64) / (dt as f64));
        }
    }
    let dt = st.elapsed_ms();
    info!("Done: ran {} steps in {} ms ({} steps/s).",
        num, dt, 1000.0 * (num as f64) / (dt as f64));

    let st5 = Stopwatch::start_new();
    let output = format!("{}-best.vcf.gz", a_prefix);
    c.write_results_vcf(&output, true).expect("writing best candidates");
    info!("written best candidates to {:?} in {} ms", output, st5.elapsed_ms());
}
