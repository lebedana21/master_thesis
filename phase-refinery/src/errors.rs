quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Io(err: ::std::io::Error) {
            from()
            description(err.description())
        }
        Msg(err: String) {
            from()
            description("message-only error")
        }
        Other(err: Box<::std::error::Error>) {
            cause(&**err)
            description(err.description())
        }
    }
}

impl<'a> From<&'a str> for Error {
    fn from(s: &'a str) -> Self {
        Error::Msg(s.to_string())
    }
}

pub type Result<T> = ::std::result::Result<T, Error>;

pub trait OptErr<T, E: Into<Error>> {
    fn some_or(self, err: E) -> Result<T>;
}

impl<T, E: Into<Error>, EIn: Into<Error>> OptErr<T, E> for
Option<::std::result::Result<T, EIn>> {
    fn some_or(self, err: E) -> Result<T> {
        match self {
            None => Err(err.into()),
            Some(x) => match x {
                Ok(x) => Ok(x),
                Err(e) => Err(e.into())
            }
        }
    }
}

pub fn other_err<E: 'static + ::std::error::Error>(e: E) -> Error {
    Error::Other(Box::new(e))
}

pub trait BoxErr<T> {
    fn box_err(self: Self) -> Result<T>;
}

impl<T, E: 'static + ::std::error::Error> BoxErr<T> for ::std::result::Result<T, E> {
    fn box_err(self) -> Result<T> { self.map_err(|e| other_err(e)) }
}