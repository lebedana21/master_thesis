use std::collections::HashMap;
use std::io::{BufReader, BufRead};
use std::fs::File;
use std::str::FromStr;
use std::process;

#[derive(Debug, PartialEq)]
pub struct Read {
    pub name: String,
    pub alleles_pos: Vec<u32>,
    pub alleles_posteriori: Vec<(u32, u32)>,
}
impl Read {
    pub fn new(rname: String) -> Self {
        Read {
            name: rname,
            alleles_pos: Vec::new(),
            alleles_posteriori: Vec::new()
        }
    }

    pub fn to_hashMap(&self) -> HashMap<u32, (u32, u32)> {
        let mut pos_to_poster = HashMap::new();
        for i in 0..self.alleles_posteriori.len() {
            pos_to_poster.insert(self.alleles_pos[i], self.alleles_posteriori[i].clone());
        }
        return pos_to_poster.clone()
    }

    pub fn elongate(&mut self, pos_next: u32, posteriori_tuple_next: (u32, u32))  {
        self.alleles_pos.push(pos_next);
        self.alleles_posteriori.push(posteriori_tuple_next);
    }
}
impl Clone for Read {
    fn clone(&self) -> Self {
        Read {
            name: self.name.clone(),
            alleles_pos: self.alleles_pos.clone(),
            alleles_posteriori: self.alleles_posteriori.clone(),
        }
    }
}
// Read .var and initialize read panel instance
pub fn readpanel_fromfile(fpath: Option<&str>) -> HashMap<u32, Vec<Read>> {
    let mut read_panel: HashMap<u32, Vec<Read>> = HashMap::new();
    // return empty hashmap if path was not specified
    if fpath.is_none() { return read_panel; }
    // Parse input and fill the panel
    let file = File::open(fpath.unwrap()).unwrap();
    let lines = BufReader::new(file).lines();
    for lr in lines {
        let line = lr.unwrap();
        let mut l = line.split_whitespace();
        // get read's parameters
        let rname = l.next().unwrap().to_string(); // read id
        let mut rstart_pos: u32 = 0; let mut start_pos_flag: bool = true;
        let rcovered_pos = l.next().unwrap().split(":");
        let mut rposteriori = l.next().unwrap().split(":");
        let mut read_inst = Read::new(rname.clone());
        for rpos_str in rcovered_pos {
            let rpos = u32::from_str(rpos_str).unwrap() - 1;
            let pos_posteriori_str = rposteriori.next().unwrap().to_string();
            let mut poster_ref:u32 = 0;
            let mut poster_allele:u32 = 0;
            let mut pos_posteriori = pos_posteriori_str.split(",");
            // do not add multiallelic variants
            if pos_posteriori.clone().count() > 2 {continue;}
            if start_pos_flag {
                // set up first read's positions
                rstart_pos = rpos.clone();
                start_pos_flag = false;
            }
            // if it is read's inner part
            if pos_posteriori_str == "-1" {
                poster_ref = 0;
                poster_allele = 0;
            }else {
                poster_ref = u32::from_str(pos_posteriori.next().unwrap()).unwrap();
                poster_allele = u32::from_str(pos_posteriori.next().unwrap()).unwrap();
            }
            // insert tuple with pos. as a key
            if start_pos_flag {panic!("Read's start position was not set up. Corresponding line in .var is {:?}", line)}
            read_inst.elongate(rpos, (poster_ref, poster_allele));
        }
        if !read_panel.contains_key(&rstart_pos) {
            read_panel.insert(rstart_pos, Vec::new());
        }
        read_panel.get_mut(&rstart_pos).unwrap().push(read_inst.clone());
    }
    return read_panel
}
