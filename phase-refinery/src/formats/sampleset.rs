use rust_htslib::bcf;
use pbwt::BitVec;
use rust_htslib::bcf::record::GenotypeAllele;
use std::cmp::min;
use owning_ref::OwningRefMut;
use std::path::Path;
use std::fmt;
#[allow(unused_imports)]
use super::*;
use ::*;

pub struct SampleSet {
    /// Flag to forget phasing information on read (for re-phasing)
    pub forget_phase: bool,
    /// Positions already read
    pub positions: Vec<Index>,
    /// alleles already read, index as alleles[location][alle_no]
    pub alleles: Vec<Vec<String>>,
    /// Indexes of samples being read (two values read for each sample)
    pub samples: Vec<Index>,
    /// Names of samples being read (two values read for each sample)
    pub sample_names: Vec<String>,
    /// BCF file reader
    pub reader: Box<bcf::Reader>,
    /// Records from the reader above -- manual and unsafe :-(
    pub records: bcf::Records<'static>,
}

impl fmt::Debug for SampleSet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("RefPanel")
         .field("positions", &self.positions)
         .field("samples", &self.samples)
         .field("sample_names", &self.sample_names)
         .finish()
    }
}

#[derive(Clone, Debug)]
pub struct SampleSetRecord {
    pub num: Index,
    pub pos: Index,
    /// Sample genotypes, two for each read sample (the length is `2 * samples.len()`).
    /// Here either both are `Phased` or both `Unphased`.
    pub gt: Vec<GenotypeAllele>,
    /// (unused for now)
    pub probs: Vec<i32>, 
}

fn gt_phase(gt: GenotypeAllele) -> GenotypeAllele {
    match gt {
        GenotypeAllele::Unphased(v1) => GenotypeAllele::Phased(v1),
        GenotypeAllele::UnphasedMissing => GenotypeAllele::PhasedMissing,
        phased => phased,
    }
}

fn gt_unphase(gt: GenotypeAllele) -> GenotypeAllele {
    match gt {
        GenotypeAllele::Phased(v1) => GenotypeAllele::Unphased(v1),
        GenotypeAllele::PhasedMissing => GenotypeAllele::UnphasedMissing,
        unphased => unphased,
    }
}

fn is_phased(gt: GenotypeAllele) -> bool {
    match gt {
        GenotypeAllele::Phased(_) => true,
        GenotypeAllele::PhasedMissing => true,
        _ => false,
    }
}

impl<'a> Iterator for &'a mut SampleSet {
    type Item = Result<SampleSetRecord>;

    fn next(&mut self) -> Option<Self::Item> {
        let r = self.records.next();
        match r {
            Some(recres) => {
                let mut rec: bcf::Record = recres.unwrap();
                let mut res = SampleSetRecord {
                    num: self.positions.len() as Index,
                    pos: rec.pos() as Index,
                    gt: Vec::new(),
                    probs: Vec::new(),
                };
                self.positions.push(rec.pos() as Index);
                self.alleles.push(rec.alleles().iter().map(
                        |&al| String::from_utf8(Vec::from(al)).unwrap()
                    ).collect());
                let gts = match rec.genotypes().box_err() {
                    Err(e) => return Some(Err(e.into())),
                    Ok(v) => v };
                for mut gt in self.samples.iter().map(|i| gts.get(*i as usize)) {
                    assert_eq!(gt.len(), 2);
                    if (!self.forget_phase) && (is_phased(gt[0]) || is_phased(gt[1])) {
                        res.gt.push(gt_phase(gt[0]));
                        res.gt.push(gt_phase(gt[1]));
                    } else {
                        res.gt.push(gt_unphase(gt[0]));
                        res.gt.push(gt_unphase(gt[1]));
                    }
                }
                Some(Ok(res))
            }
            None => None,
        }        
    }
}

impl SampleSet {
    pub fn read<P: AsRef<Path>>(path: &P, samples_: &Option<Vec<Index>>, forget_phase: bool) ->
            Result<Self> {
        let mut r = Box::new(bcf::Reader::from_path(path).box_err()?);
        let mmax: Index = r.header().sample_count() as Index;
        let samples: Vec<Index> = match samples_ {
            &None => (0..mmax as Index).collect(),
            &Some(ref it) => it.iter().cloned().filter(|x| *x < mmax).collect(),
        };
        let sample_names: Vec<_> = {
            let all_sample_names = r.header().samples();
            samples.iter().map(
                |i| String::from_utf8_lossy(all_sample_names[*i as usize]).into_owned()).collect()
        };
        let ptr: *mut rust_htslib::bcf::Reader = r.as_mut();
        let records = unsafe { (*ptr).records() };
        Ok(SampleSet {
            positions: Vec::new(),
            alleles: Vec::new(),
            samples: samples,
            sample_names: sample_names,
            records: records,
            reader: r,
            forget_phase: forget_phase,
        })
    }

    /// Number of reference samples
    pub fn m(&self) -> usize {
        self.sample_names.len()
    }

    pub fn iter<'a>(&mut self) -> &mut Self {
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_panel() {
        let mut panel = SampleSet::read(&"../ex-chr20/ref-chr20.bcf", &None, false).unwrap();
        assert_eq!(panel.samples.len(), 2405);
        assert_eq!(panel.sample_names.len(), 2405);
        for (i, recres) in panel.iter().take(20).enumerate() {
            let res = recres.unwrap();
            assert_eq!(res.gt.len(), 4810);
            assert_eq!(res.num as usize, i);
        }
        assert_eq!(panel.alleles.len(), 20);
        assert_eq!(panel.alleles[0].len(), 2);
        assert_eq!(panel.positions.len(), 20);
    }
}
