use std::vec::Vec;
use std::convert::AsRef;
use std::path::Path;
use std::io::{BufReader, BufRead, Write, BufWriter};
use std::fs::File;
use std::str::FromStr;
use std::iter::Iterator;
use std::collections::HashMap;

use rust_htslib::bcf;
use rust_htslib::bcf::record::GenotypeAllele;

use flate2::write::GzEncoder;
use flate2::Compression;

#[allow(unused_imports)]
use super::*;
use ::*;
use pbwt::BitVec;
use utils::*;

pub fn write_candidates_vcf_stream<W: Write>(
        w: &mut W, set: &SampleSet, gts: &[GenoPair], unphased: &HashMap<Index, Vec<GenotypeAllele>>) -> Result<()> {
    let m = gts[0].len();
    let n = set.positions.len();
    assert_eq!(gts.len(), set.m());
    assert_eq!(gts[0].len() + unphased.len(), n);
    write!(w, "##fileformat=VCFv4.2\n")?;
    write!(w, "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n")?;
    write!(w, "##reference=file:///lustre/scratch116/vr/ref/human/GRCh37/hs37d5.fa\n")?;
    write!(w, "##contig=<ID=20,length=63025520>\n")?;
    write!(w, "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT")?;
    for ref name in &set.sample_names {
        write!(w, "\t{}", name)?;
    }
    write!(w, "\n")?;
    let mut gts_index = m - 1;
    for (i, pos) in set.positions.iter().enumerate() {
        write!(w, "20\t{}\t.\t{}\t{}\t.\t.\t.\tGT",
               pos + 1, set.alleles[i][0], set.alleles[i][1])?; // 1-based pos indexes!
        match unphased.get(&(i as Index)) {
            Some(ref gtvec) => {
                for samp in 0 .. set.m() {
                    let gt: [String; 2] = map01(|x|
                        match gtvec[2 * samp + x] {
                            GenotypeAllele::Unphased(i) => i.to_string(),
                            GenotypeAllele::UnphasedMissing => ".".into(),
                            _ => panic!("unexpected phased GT allele in `unphased`"),
                        }
                    );
                    if gt[0] == gt[1] {
                        // Post-phase anything with equal haplotypes
                        write!(w, "\t{}|{}", gt[0], gt[1])?;
                    } else {
                        write!(w, "\t{}/{}", gt[0], gt[1])?;
                    }
                }
            }
            None => {
                for samp in 0 .. set.m() {
                    let gt = gts[samp].get(gts_index);
                    write!(w, "\t{}|{}", gt[0], gt[1])?;
                }
                // Overflows on the last step, but should never be read from afterwards
                gts_index = gts_index.wrapping_sub(1);
            }
        }
        write!(w, "\n")?;
    }
    Ok(())
}

pub fn write_candidates_vcf<P: AsRef<Path>>(
    path: P, set: &SampleSet, gts: &[GenoPair], unphased: &HashMap<Index, Vec<GenotypeAllele>>, gzip: bool) -> Result<()> {
    let file = File::create(path)?;
    if gzip {
        write_candidates_vcf_stream(&mut GzEncoder::new(file, Compression::best()),
                                    set, gts, unphased)
    } else {
        write_candidates_vcf_stream(&mut BufWriter::new(file), 
                                    set, gts, unphased)
    }
}

