#![allow(unused_imports)]
#![feature(const_fn)]
#![feature(test)]
#![feature(const_min_value)]
#![feature(underscore_lifetimes)] 

#[macro_use] extern crate quick_error;
#[macro_use] extern crate lazy_static;
extern crate num;
extern crate rust_htslib;
#[macro_use] extern crate itertools;
#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate stopwatch;
extern crate rayon;
extern crate pbwt;
extern crate flate2;
extern crate owning_ref;

#[cfg(test)]
extern crate test;

pub mod phasing;
pub use phasing::{Computation, ComputationCfg, State};

pub mod lognum;
pub use lognum::{LogFixPoint, LogFixPointType, LogF64};

pub mod utils;
pub use utils::{GenoPair, GenoPairIter, WeightedStat, ChainBitVec, ChainBitVecIter, SimpleHash};

pub mod formats;
pub use formats::{SampleSet, SampleSetRecord, GeneMap};

pub mod errors;
pub use errors::*;

/// Type for likelyhood computation
pub type Likelyhood = lognum::LogFixPoint;

/// Type of location value in the genome and variant index
/// Type of sample (or reference) index in the reference panel
pub type Index = u32;

/// Genotype variant value
/// Non-negative values (0, 1, ...) are states, -1 a missing value
pub type Variant = i8;

/// Pair of variants in a location of a genotype
pub type VariantPair = [Variant; 2];
