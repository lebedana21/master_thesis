use std::ops::{Mul, Div, Add};
use std::iter::Sum;
use std::fmt;
use std::cmp::{Ord, Eq, PartialOrd, Ordering};
use num::{Float, Integer, Num, Bounded, NumCast};

pub type LogFixPointType = i32;

/// Number specified only by its fixed-point-represented exponent.
/// The exponent is base 10 and the fixed point has 3 decimals after the period.
/// Zero is represented using the minimal value. Negative numbers, infinity or NaNs are not allowed.
#[derive(Copy, Clone, PartialOrd, Ord, PartialEq, Eq, Serialize, Deserialize)]
pub struct LogFixPoint(LogFixPointType);

// TODO: specialize f32 ops for speed

impl LogFixPoint {
    #[inline]
    const fn multiplier() -> LogFixPointType { 1000 }

    #[inline]
    const fn zero_exp() -> LogFixPointType { LogFixPointType::min_value() }

    #[inline]
    pub const fn zero() -> Self {
        LogFixPoint(Self::zero_exp())
    }

    #[inline]
    pub const fn one() -> Self {
        LogFixPoint(0)
    }

    #[inline]
    pub fn from_float(p: f64) -> Self {
        Self::from_log10(p.log10())
    }

    /// Constructs `LogFixPoint` directly from given `1000 * log_10(x)`.
    pub const fn from_log10_milis(milis: LogFixPointType) -> Self {
        LogFixPoint(milis)
    }

    pub fn from_log10(l: f64) -> Self {
        if l.is_infinite() && l.is_sign_negative() {
            return Self::zero();
        }
        assert!(l.is_finite());
        if (l * Self::multiplier() as f64) < (Self::zero_exp() + 2) as f64 {
            return Self::zero();
        }
        if l * Self::multiplier() as f64 > (LogFixPointType::max_value() - 2) as f64 {
            panic!(format!("Overflow on float {:?} to integer conversion.", l));
        }
        LogFixPoint((l * Self::multiplier() as f64) as LogFixPointType)
    }

    pub fn to_float(self) -> f64 {
        if self.0 == Self::zero_exp() {
            0.0
        } else {
            10.0f64.powf(self.to_log10())
        }
    }

    pub fn to_log10(self) -> f64 {
        if self.0 == Self::zero_exp() {
            f64::neg_infinity()
        } else {
            (self.0 as f64) / Self::multiplier() as f64
        }
    }

    #[allow(dead_code)]
    pub(self) fn add_through_float(self, other: Self) -> Self {
        if self == Self::zero() { return other; }
        if other == Self::zero() { return self; }
        let (a, b) = if self.0 <= other.0 {
            (self.0, other.0)
        } else {
            (other.0, self.0)
        };
        // a <= b
        match a.checked_sub(b) {
            None => LogFixPoint(b),
            Some(d) if d < -2637 => LogFixPoint(b),
            Some(d) => LogFixPoint(
                b + (1000.0 * (1.0 + 10.0.powf(d as f32 / 1000.0)).log10()) as
                        LogFixPointType,
            ),
        }
    }

    pub(self) fn add_through_table(self, other: Self) -> Self {
        if self == Self::zero() { return other; }
        if other == Self::zero() { return self; }
        let (a, b) = if self.0 <= other.0 {
            (self.0, other.0)
        } else {
            (other.0, self.0)
        };
        // a <= b
        match a.checked_sub(b) {
            None => LogFixPoint(b),
            Some(d) if d < -2637 => LogFixPoint(b),
            Some(d) => LogFixPoint(b + LOG_FIX_ADD_TABLE[(-d) as usize]),
        }
    }
}

impl Default for LogFixPoint {
    #[inline]
    fn default() -> Self { LogFixPoint::one() }
}

impl fmt::Debug for LogFixPoint {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "L=10^{:.3}", self.to_log10())
    }
}

impl fmt::Display for LogFixPoint {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "10^{:.3}", self.to_log10())
    }
}

impl Mul for LogFixPoint {
    type Output = Self;

    #[inline]
    fn mul(self, other: Self) -> Self {
        if self == Self::zero() || other == Self::zero() {
            return Self::zero();
        }
        match self.0.checked_add(other.0) {
            Some(x) => LogFixPoint(x),
            None => {
                if self.0 < 0 {
                    Self::zero()
                } else {
                    panic!("Integer overlflow in ExpFixPoint mul()");
                }
            }
        }
    }
}

impl Mul<f64> for LogFixPoint {
    type Output = Self;

    #[inline]
    fn mul(self, other: f64) -> Self {
        self * Self::from_float(other)
    }
}

impl Div for LogFixPoint {
    type Output = Self;

    #[inline]
    fn div(self, other: Self) -> Self {
        if other == Self::zero() {
            panic!("Division by zero in ExpFixPoint div()");
        }
        self * LogFixPoint(-other.0)
    }
}

impl Div<f64> for LogFixPoint {
    type Output = Self;

    #[inline]
    fn div(self, other: f64) -> Self {
        self / Self::from_float(other)
    }
}

lazy_static! {
    static ref LOG_FIX_ADD_TABLE: Vec<LogFixPointType> = {
        (0..3000)
            .map(|d| (1000.0 * (1.0 + 10.0.powf(-d as f64 / 1000.0)).log10())
                     as LogFixPointType)
            .collect()
    };
}

impl Add for LogFixPoint {
    type Output = Self;

    #[inline]
    fn add(self, other: Self) -> Self {
        self.add_through_table(other)
    }
}

impl Add<f64> for LogFixPoint {
    type Output = Self;

    #[inline]
    fn add(self, rhs: f64) -> Self {
        self.add(LogFixPoint::from_float(rhs))
    }
}

impl Add<f32> for LogFixPoint {
    type Output = Self;

    #[inline]
    fn add(self, rhs: f32) -> Self {
        self.add(LogFixPoint::from_float(rhs as f64))
    }
}

impl Sum for LogFixPoint {
    #[inline]
    fn sum<I: Iterator<Item = LogFixPoint>>(iter: I) -> Self {
        iter.fold(LogFixPoint::from_float(0.0), |a, b| a + b)
    }
}

impl<'a> Sum<&'a LogFixPoint> for LogFixPoint {
    #[inline]
    fn sum<I: Iterator<Item = &'a LogFixPoint>>(iter: I) -> Self {
        iter.fold(LogFixPoint::from_float(0.0), |a, b| a + *b)
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::{Bencher, black_box};

    fn apx_eq(a: f64, b: f64) -> bool {
        (a / b) < 1.005 && (a / b) > 0.995
    }

    #[test]
    fn test_ops() {
        let l1 = LogFixPoint::from_float(0.05);
        assert!(apx_eq((l1 * 2.0).to_float(), 0.1));
        assert!(apx_eq((l1 + LogFixPoint::from_float(0.1)).to_float(), 0.15));
        let l2 = LogFixPoint::from_float(1e-20);
        assert!(apx_eq((l1 + l2).to_float(), 0.05));
        assert!(apx_eq((l2 + l1).to_float(), 0.05));
        assert!(apx_eq((l2 * l1).to_float(), 5e-22));
        assert!(apx_eq((l1 / l2).to_float(), 5e18));
        assert!(LogFixPoint::from_log10(-2.0).to_log10() == -2.0);
    }

    #[test]
    fn test_disp() {
        let l1 = LogFixPoint::from_float(0.05);
        assert_eq!(format!("{}", l1), "10^-1.301");
        assert_eq!(format!("{:?}", l1), "L=10^-1.301");
    }

    #[test]
    fn test_sum() {
        let l1 = LogFixPoint::from_float(0.0);
        assert_eq!(l1.to_float(), 0.0);
        let ls1: &[LogFixPoint] = &[
            LogFixPoint::from_float(0.1),
            LogFixPoint::from_float(0.0),
            LogFixPoint::from_float(0.2),
        ];
        assert!(apx_eq(ls1.iter().sum::<LogFixPoint>().to_float(), 0.3));
        let ls2: &[LogFixPoint] = &[][..];
        assert!(ls2.iter().sum::<LogFixPoint>().to_float().abs() < 1e-20);
    }

    #[test]
    fn test_eq_cmp() {
        let l0 = LogFixPoint::from_float(0.0);
        let l1 = LogFixPoint::from_float(1.0);
        let l2 = LogFixPoint::from_float(2.0);
        assert_eq!(l0, l0);
        assert_eq!(l1, l1);
        assert!(l0 < l1);
        assert!(l1 < l2);
        assert!(l2 > l0);
    }

    fn test_adds(a: f64, b:f64) {
        let fa = LogFixPoint::from_float(a);
        let fb = LogFixPoint::from_float(b);
        assert!(apx_eq(a + b, (fa + fb).to_float()));
        assert_eq!(fa.add_through_table(fb), fa + fb);
        assert_eq!(fa.add_through_float(fb), fa.add_through_table(fb));
    }

    #[test]
    fn test_add_with_table() {
        for i in 0 .. 10000 {
            test_adds(1.0, i as f64);
            test_adds(1000.0, i as f64);
            test_adds(100000.0, i as f64);
            test_adds(1.0, (i as f64) * 0.001);
            test_adds(1.0, (i as f64) * 0.000001);
        }
    }

    #[bench]
    fn bench_add_1000_f64(b: &mut Bencher) {
        b.iter(|| {
            let mut a = black_box(42.0f64);
            let b = black_box(1.0f64);
            for _i in 0..1000 {
                a += b;
            }
            a
        })
    }

    #[bench]
    fn bench_add_1000_fix_via_float(b: &mut Bencher) {
        b.iter(|| {
            let mut a = black_box(LogFixPoint::from_float(42.0f64));
            let b = black_box(LogFixPoint::from_float(1.0f64));
            for _i in 0..1000 {
                a = a.add_through_float(b);
            }
            a
        })
    }

    #[bench]
    fn bench_add_1000_fix_via_table(b: &mut Bencher) {
        b.iter(|| {
            let mut a = black_box(LogFixPoint::from_float(42.0f64));
            let b = black_box(LogFixPoint::from_float(1.0f64));
            for _i in 0..1000 {
                a = a.add_through_table(b);
            }
            a
        })
    }
}
