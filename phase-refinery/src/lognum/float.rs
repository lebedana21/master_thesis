use std::ops::{Mul, Div, Add};
use ::std::iter::Sum;
use std::fmt;
use std::cmp::{Ord, Eq, PartialOrd, Ordering};
use num::{Float, Integer, Num, Bounded, NumCast};


/// Log-scale floating point numbers consisting of one f64.
/// This type uses 10-base logarithm.
#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct LogF64 {
    log10: f64
}

impl LogF64 {
    pub fn from_float(p: f64) -> Self { LogF64 { log10: p.log10() } }
    pub const fn from_log10(l: f64) -> Self { LogF64 { log10: l } }
    pub fn to_float(self) -> f64 {
        if self.log10.is_infinite() {
            assert!(self.log10.is_sign_negative());
            return 0.0;
        } else { 10.0f64.powf(self.log10) }
    }
    pub fn to_log10(self) -> f64 { self.log10 }
}

impl fmt::Debug for LogF64 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "L=10^{}", self.log10)
    }
}

impl fmt::Display for LogF64 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "10^{}", self.log10)
    }
}

impl Mul for LogF64 {
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        LogF64 { log10: self.log10 + other.log10 }
    }
}

impl Mul<f64> for LogF64 {
    type Output = Self;
    fn mul(self, other: f64) -> Self {
        self * LogF64::from_float(other)
    }
}

impl Div for LogF64 {
    type Output = Self;
    fn div(self, other: Self) -> Self {
        LogF64 { log10: self.log10 - other.log10 }
    }
}

impl Div<f64> for LogF64 {
    type Output = Self;
    fn div(self, other: f64) -> Self {
        self / LogF64::from_float(other)
    }
}

impl Add<f64> for LogF64 {
    type Output = LogF64;
    fn add(self, rhs: f64) -> Self::Output {
        self.add(LogF64::from_float(rhs))
    }
}

impl Add for LogF64 {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        if self.log10.is_infinite() {
            assert!(self.log10.is_sign_negative());
            return other;
        }
        if other.log10.is_infinite() {
            assert!(other.log10.is_sign_negative());
            return self;
        }
        let d = self.log10 - other.log10;
        let l = if d < 0.0 {
            self.log10 + (1.0 + 10.0f64.powf(-d)).log10()
        } else {
            other.log10 + (1.0 + 10.0f64.powf(d)).log10()
        };
        LogF64 { log10: l }
    }
}

impl Ord for LogF64 {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.log10 == other.log10 {
            return Ordering::Equal
        }
        if self.log10.is_infinite() {
            debug_assert!(self.log10.is_sign_negative());
            return Ordering::Less;
        }
        if other.log10.is_infinite() {
            debug_assert!(other.log10.is_sign_negative());
            return Ordering::Greater;
        }
        debug_assert!(self.log10.is_finite() && other.log10.is_finite());
        self.log10.partial_cmp(&other.log10).unwrap()
    }
}

impl PartialOrd for LogF64 {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl PartialEq for LogF64 {
    fn eq(&self, other: &Self) -> bool { self.log10 == other.log10 }
}

impl Eq for LogF64 {}

impl Default for LogF64 {
    fn default() -> Self {
        LogF64::from_float(0.0)
    }
}

impl Sum for LogF64 {
    fn sum<I: Iterator<Item=LogF64>>(iter: I) -> Self {
        iter.fold(LogF64::from_float(0.0), |a, b| a + b)
    }
}

impl<'a> Sum<&'a LogF64> for LogF64 {
    fn sum<I: Iterator<Item=&'a LogF64>>(iter: I) -> Self {
        iter.fold(LogF64::from_float(0.0), |a, b| a + *b)
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::{Bencher, black_box};

    fn apx_eq(a: f64, b: f64) -> bool {
        (a / b) < 1.0001 && (a / b) > 0.9999
    }

    #[test]
    fn test_ops() {
        let l1 = LogF64::from_float(0.05);
        assert!(apx_eq((l1 * 2.0).to_float(), 0.1));
        assert!(apx_eq((l1 + LogF64::from_float(0.1)).to_float(), 0.15));
        let l2 = LogF64::from_float(1e-20);
        assert!(apx_eq((l1 + l2).to_float(), 0.05));
        assert!(apx_eq((l2 + l1).to_float(), 0.05));
        assert!(apx_eq((l2 * l1).to_float(), 5e-22));
        assert!(apx_eq((l1 / l2).to_float(), 5e18));
        assert!(LogF64::from_log10(-2.0).to_log10() == -2.0);
    }

    #[test]
    fn test_disp() {
        let l1 = LogF64::from_float(0.05);
        assert!(format!("{}", l1).starts_with("10^-1.3010"));
        assert!(format!("{:?}", l1).starts_with("L=10^-1.3010"));
    }

    #[test]
    fn test_sum_p0() {
        let l1 = LogF64::from_float(0.0);
        assert_eq!(l1.to_float(), 0.0);
        let ls1: &[LogF64] = &[LogF64::from_float(0.1), LogF64::from_float(0.0), LogF64::from_float(0.2)];
        assert!(apx_eq(ls1.iter().sum::<LogF64>().to_float(), 0.3));
        let ls2: &[LogF64] = &[][..];
        assert!(ls2.iter().sum::<LogF64>().to_float().abs() < 1e-20);
    }

    #[test]
    fn test_eq_cmp() {
        let l0 = LogF64::from_float(0.0);
        let l1 = LogF64::from_float(1.0);
        let l2 = LogF64::from_float(2.0);
        assert_eq!(l0, l0);
        assert_eq!(l1, l1);
        assert!(l0 < l1);
        assert!(l1 < l2);
        assert!(l2 > l0);
    }

    #[bench]
    fn bench_add_1000_logf64(b: &mut Bencher) {
        b.iter(|| {
            let mut a = black_box(LogF64::from_float(42.0f64));
            let b = black_box(LogF64::from_float(1.0f64));
            for _i in 0..1000 {
                a = a + b;
            }
            a
        })
    }

}