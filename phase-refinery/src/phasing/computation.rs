#![allow(dead_code)]
#![allow(unused_imports)]

use itertools::Itertools;
use std::process;
use std::vec::Vec;
use std::cmp::max;
use rayon::prelude::*;
use std::fmt::Write;
use std::cmp::{Ordering, PartialOrd};
use std::iter::Iterator;
use pbwt::*;
use std::collections::HashMap;
use rust_htslib::bcf::record::GenotypeAllele;
use formats::Read;
use std::iter::FromIterator;
use std::mem;
use std::path::Path;
use std::f64;
use num::Float;
use super::super::formats::write_candidates_vcf;

use super::super::*;
use super::*;
use utils::*;

pub const MIN_CROSSOVER_L: f64 = 1e-6;

#[derive(Debug)]
pub struct Computation {
    /// Samples used as the panel (assumed phased)
    pub panel: SampleSet,
    /// PBWT structure over `panel`
    pbwt: PBWTState,
    /// Genetic map for crossover probabilities
    gene_map: GeneMap,
    /// Samples to be phased
    pub sampleset: SampleSet,
    /// State sets for every sample from the above set
    pub samples: Vec<Sample>,
    /// Computation configuration
    pub cfg: ComputationCfg,
    /// Variants that are net phased by us, indexed by index in sample set
    pub unphased: HashMap<Index, Vec<GenotypeAllele>>,
    pub statePosPrev: u32,
}

#[derive(Debug, Clone)]
pub struct Sample {
    pub states: Vec<State>,
    pub name: String,
    /// reads (name, (pos_ref,post_alt), <var_pos: (poster_ref, poster_alt)>) covering current position
    pub readvec_curr_pos: Vec<(String, [Likelyhood; 2], HashMap<u32, (u32, u32)>)>,
    // corresponds to readvec_curr_pos
    pub readpos_last: Vec<u32>,
    /// all read as <start_pos: Vec<Read>>
    pub readpanel: HashMap<u32, Vec<Read>>,
}

impl Sample {
    // Set current pos probabilities for continuing reads and for reads starting in the position
    pub fn update_readvec(&mut self, pos_next: &Index) -> Vec<usize> {
        // converts int tuple to float list, following: float = 10^(int/-10)
        fn log2_likelihood(post_int: &(u32, u32)) -> [Likelyhood; 2] {
            let mut p_ref: Likelyhood = Likelyhood::from_float(1.0);
            let mut p_alt: Likelyhood = Likelyhood::from_float(1.0);
            let mut p_ref_float: f64;
            let mut p_alt_float: f64;
            if post_int.0 != 0 { // to avoid converting
                p_ref_float = 10.0.powf((post_int.0 as f64)/(-10.0));
                p_ref = Likelyhood::from_float(p_ref_float);
            }
            if post_int.1 != 0 { // to avoid converting
                p_alt_float = 10.0.powf((post_int.1 as f64)/(-10.0));
                p_alt = Likelyhood::from_float(p_alt_float);
            }
            return [p_ref, p_alt]
        }
        let mut reads_next: Vec<(String, [Likelyhood; 2], HashMap<u32, (u32, u32)>)> = Vec::new();
        let mut idx_tokeep: Vec<usize> = Vec::new();
        let mut readpos_last_next: Vec<u32> = Vec::new();

        for i in 0..self.readvec_curr_pos.len() {
            // keep reads which continue
            let read = &self.readvec_curr_pos[i];
            let mut read_updated: (String, [Likelyhood; 2], HashMap<u32, (u32, u32)>) = read.clone();
            // if read has variant at the position
            if read.2.contains_key(pos_next) {
                read_updated.1 = log2_likelihood(read_updated.2.get(&pos_next).unwrap());
                // if read doesnt have variant at the position, but it covers the position
            } else if &self.readpos_last[i] > pos_next {
                read_updated.1 = [Likelyhood::from_float(1.0), Likelyhood::from_float(1.0)];
                // skip the read  if it doesn't cover the variant
            } else { continue; }
            // store read
            reads_next.push(read_updated); // store the read
            idx_tokeep.push(i); // store it's id in reads_curr vec.
            readpos_last_next.push(self.readpos_last[i]);
        }

        // get reads which first position is the current position and append it to the `reads_next`
        let reads_starting = self.readpanel.get_mut(&pos_next);
        if reads_starting.is_some() {
            for read in reads_starting.unwrap() {
                reads_next.push((read.name.clone(), log2_likelihood(&read.alleles_posteriori[0]), read.to_hashMap()));
                readpos_last_next.push(read.alleles_pos[read.alleles_pos.len()-1]);
            }
        }
        self.readvec_curr_pos = reads_next; // substitute
        self.readpos_last = readpos_last_next;
        return idx_tokeep // return indexes of reads which continue
    }
}

#[derive(Debug, Clone, Default)]
pub struct ComputationCfg {
    /// Number of states to keep after pruning
    pub limit_states: usize,
    /// Unused
    pub new_var_prob: Likelyhood,
    /// Mean IBD length (`a` in the Eagle paper)
    pub mean_ibd: f64,
    /// Use state likelihood to select the best candidate (only in the last step)
    pub use_state_likelihood: bool,
}

impl Computation {
    pub fn new(panel: SampleSet,
               sampleset: SampleSet,
               gene_map: GeneMap,
               cfg: ComputationCfg,
               readpanel: HashMap<u32, Vec<Read>>,
               statePosPrev: u32) -> Self {
        let pbwt = PBWTState::new(panel.m() * 2);
        let ref0 = PBWTCursor::new_full_range(&pbwt);
        Computation {
            panel: panel,
            pbwt: pbwt,
            gene_map: gene_map,
            samples: sampleset.sample_names
                              .iter()
                              .map(|name| 
                                    Sample { 
                                        states: vec![State::new(ref0.clone())],
                                        name: name.clone(),
                                        readvec_curr_pos: Vec::new(),
                                        // the same panel for each sample so far
                                        readpanel: readpanel.clone(),
                                        readpos_last: Vec::new(),
                                    })
                              .collect(),
            sampleset: sampleset,
            cfg: cfg,
            unphased: HashMap::new(),
            statePosPrev: 1
        }
    }

    fn best_states_by<'a, T: PartialOrd, F: Fn(&State) -> T>(&'a self, f: F) ->
            Vec<&'a State> {
        self.samples.iter().map(|sample|
            sample.states.iter().max_by(
                |a, b| PartialOrd::partial_cmp(&f(&a), &f(&b))
                    .expect("Invalid float values to compare.")
            ).expect("a sample with no states")
        ).collect()
    }
 
    /// Return the states with the highest `cand_l`.
    pub fn best_candidates<'a>(&'a self) -> Vec<&'a State> {
        self.best_states_by(|s| s.cand_l)
    }

    /// Return the states with highest `state_l`.
    pub fn best_states<'a>(&'a self) -> Vec<&'a State> {
        self.best_states_by(|s| s.state_l)
    }

    pub fn step(&mut self) -> Result<bool> {
        let mut sample_r = None;
        let mut panel_r = None;
        while sample_r.is_none() || panel_r.is_none() {
            if panel_r.is_none() {
                panel_r = match (&mut self.panel).next() {
                    Some(rec) => Some(rec?),
                    _ => return Ok(false)
                }
            }
            if sample_r.is_none() {
                sample_r = match (&mut self.sampleset).next() {
                    Some(rec) => Some(rec?),
                    _ => return Ok(false)
                }
            }
            let panel_pos = panel_r.as_ref().unwrap().pos;
            let panel_num = panel_r.as_ref().unwrap().num;
            let sample_pos = sample_r.as_ref().unwrap().pos;
            let sample_num = sample_r.as_ref().unwrap().num;
            if self.panel.alleles[panel_num as usize].len() != 2 {
                // Multi-allelic site in panel
                // ignore it
                panel_r = None;
                continue;
            }

            let mut singleton_panel = false;
            if let Some(ref pr) = panel_r {
                let mut cnt = [0usize, 0];
                for g in &pr.gt { match g {
                    &GenotypeAllele::Phased(v) => cnt[v as usize] += 1,
                    _ => (),
                }};
                drop(pr);
                if cnt[0] < 1 || cnt[1] < 1 {
                    singleton_panel = true;
                    debug!("Panel position {} (index {}) has value count {:?}, skipped", sample_pos, sample_num, cnt);
                }
            }
            if singleton_panel {
                // Panel position only has one allelle
                panel_r = None;
                continue;
            }

            if panel_pos < sample_pos {
                // A position in panel not in the sample
                // ignore it
                panel_r = None;
                continue;
            }
            if self.sampleset.alleles[sample_num as usize].len() != 2 {
                // Multi-allelic site in samples
                // output it unphased
                let sr = sample_r.take().unwrap();
                debug!("Multi-allelic sample at position {} (index {}) skipped", sample_pos, sample_num);
                self.unphased.insert(sample_num, sr.gt);
                continue;
            }
            if panel_pos > sample_pos {
                // A position in the sample not in the panel
                // output it unphased
                let sr = sample_r.take().unwrap();
                debug!("Sample position {} (index {}) not in panel, skipped", sample_pos, sample_num);
                self.unphased.insert(sample_num, sr.gt);
                continue;
            }
            if panel_pos == self.statePosPrev {
                let sr = sample_r.take().unwrap();
                debug!("Repeated position {} (index {}) skipped", sample_pos, sample_num);
                self.unphased.insert(sample_num, sr.gt);
                continue;
            }
        }
        // Here we have matching positions, bi-allelic variants
        let panel_r = panel_r.unwrap();
        let sample_r = sample_r.unwrap();

        // Advance PBWT state
        let next_col = BitVec::from_iter(panel_r.gt.iter().map(
            |gt| match *gt {
                GenotypeAllele::Phased(i) => i != 0,
                _ => panic!("unphased or missing allele in panel at pos {}", panel_r.pos),
            }
        ));
        self.pbwt.set_next_column(next_col);
        // Update states
        for samp_i in 0..self.samples.len() {
            let gtpair = map01(|i| match sample_r.gt[samp_i * 2 + i] {
                GenotypeAllele::Phased(_) => panic!("phased alleles in samples no impl yet"),
                GenotypeAllele::Unphased(gt) => gt as Variant,
                _ => panic!("missing allele in sample"),
            });
            let read_idxs_tokeep = self.samples[samp_i].update_readvec(&sample_r.pos);
            self.step_for_sample(samp_i, sample_r.pos, sample_r.num, gtpair, read_idxs_tokeep);

            self.statePosPrev = sample_r.pos
        }
        // Advance PBWT state
        self.pbwt = self.pbwt.advance();
        Ok(true)
    }

    fn step_for_sample(&mut self, sample_index: usize, pos: Index, num: Index, gt: VariantPair, read_idxs_tokeep: Vec<usize>) {

        // Add original states (no crossover)
        let mut states_to_extend: Vec<State> = self.samples[sample_index].states.clone();
        let normalizeBy: LogFixPoint = states_to_extend[states_to_extend.len()-1].state_l;

        // Only crossover for states other than first
        if num > 0 {
            // Add one-parent crossover states
            for i in 0..2 {
                let mut collapse_i: Vec<&State> = ref_vec(&self.samples[sample_index].states);
                collapse_i.sort_by_key(|s: &&State| s.hist.hashes[1 - i]);
                for (_, group) in collapse_i
                        .into_iter()
                        .group_by(|s: &&State| s.hist.hashes[1 - i])
                        .into_iter() {
                    states_to_extend.push(self.crossover_state(pos, num, group.collect(), [i == 0, i == 1]));
                }
            }

            // Add both-parents crossover state
            states_to_extend.push(self.crossover_state(pos, num, self.samples[sample_index].states.iter().collect(), [true, true]));
        }

        let mut ext_states: Vec<State> = Vec::new();

        // Add state extensions
        // maximum likelihood from the prev step
        for s in states_to_extend {
            // println!(" -- Process state --");
            s.push_extension_with(&mut ext_states, &self.pbwt, gt,
                                  read_idxs_tokeep.clone(), normalizeBy.clone(),
                                  &self.samples[sample_index].readvec_curr_pos, pos);
        }
        assert!(ext_states[0].cand.len() > num as usize / 2);
        // Limit the number of retained states
        ext_states.sort_unstable_by_key(|s| s.state_l);
        let nsl = ext_states.len();
        if nsl > self.cfg.limit_states {
            ext_states.drain(..(nsl - self.cfg.limit_states));
        }
        //println!("Step: pos {:?}, highest L {:?} and smallest L {:?}; reads idx to keep {:?}", pos, ext_states[0].state_l, ext_states[ext_states.len()-1].state_l, read_idxs_tokeep.len());
        self.samples[sample_index].states = ext_states;
    }

    /// Create a new `State` where one or both  ories are shortened to 0
    pub fn crossover_state(&self, pos: Index, num: Index, states: Vec<&State>, erase_hist: [bool; 2]) -> State {
        assert!(states.len() > 0);
        assert_eq!(self.sampleset.positions[num as usize], pos);
        assert!(erase_hist[0] || erase_hist[1]);
        let s0 = states[0];
        let cand_tuples: Vec<(&GenoPair, Likelyhood)> = states.iter().map(|s| (&s.cand, s.cand_l)).collect();
        let (ncand_gp, ncand_l) = Self::merge_candidates(cand_tuples);
        // Genetic distance to the last location in Morgans
        let gen_len = self.gene_map.get_gmap(pos) - self.gene_map.get_gmap(self.sampleset.positions[num as usize - 1]);
        let penalties = map01(|i| if !erase_hist[i] { 1.0 } else { 
            let p: f64 = 1.0 - (-gen_len / self.cfg.mean_ibd).exp();
            if p < MIN_CROSSOVER_L { MIN_CROSSOVER_L } else { p }
        });
        let mut averaged_reads_poster: Vec<(Likelyhood, Likelyhood)> = Self::average_reads_poster(&states);
        State {
            hist: s0.hist.erase(erase_hist),
            refs: map01(|i| if erase_hist[i] {
                PBWTCursor::new_full_range(&self.pbwt)
            } else {
                s0.refs[i].clone()
            }),
            state_l: states.iter().map(|s| s.state_l).sum::<Likelyhood>() * penalties[0] * penalties[1],
            cand_l: ncand_l * penalties[0] * penalties[1],
            cand: ncand_gp.clone(),
            reads_poster_vec: averaged_reads_poster,
        }
    }

    // Averages probabilities of reads for the merged states stored in `states`
    pub fn average_reads_poster(states: &Vec<&State>) -> Vec<(Likelyhood, Likelyhood)> {
        let nreads = states.first().unwrap().reads_poster_vec.len();
        let mut averaged_reads_poster: Vec<(Likelyhood, Likelyhood)> = vec![(Likelyhood::from_float(0.0), Likelyhood::from_float(0.0)); nreads];
        // for each read average posteriori of crossovered haplotypes
        for iread in 0..nreads {
            let mut post_hap_alpha_sum:Likelyhood = Likelyhood::from_float(0.0);
            let mut post_hap_beta_sum:Likelyhood = Likelyhood::from_float(0.0);
            let mut states_likelihood_sumed:Likelyhood = Likelyhood::from_float(0.0);
            // sum up the read probs over all merging states
            for s in states.iter() {
                states_likelihood_sumed = states_likelihood_sumed.clone() + s.state_l;
                post_hap_alpha_sum = post_hap_alpha_sum.clone() + s.reads_poster_vec[iread].0*s.state_l;
                post_hap_beta_sum = post_hap_beta_sum.clone() + s.reads_poster_vec[iread].1*s.state_l;

            }
            averaged_reads_poster[iread] = (post_hap_alpha_sum/states_likelihood_sumed,
                                            post_hap_beta_sum/states_likelihood_sumed);
        }
        return averaged_reads_poster
    }


    /// Select the candidate GP with the highest likelihood,
    /// summing probabilities in case the GP is present multiple times.
    pub fn merge_candidates<'b>(mut cands: Vec<(&'b GenoPair, Likelyhood)>) ->
            (&'b GenoPair, Likelyhood) {
        cands.sort_by_key(|c| c.0.hash());
        let mut max_gp: &'b GenoPair = cands[0].0;
        let mut max_l: Likelyhood = Likelyhood::zero();
        let mut sum_l: Likelyhood = Likelyhood::zero();

        for (cur, next) in ConsPairsIter::new(cands.iter()) {
            let (gp, l) = *cur;
            if next.is_none() || gp.hash() != next.unwrap().0.hash() {
                if sum_l > max_l {
                    max_l = sum_l;
                    max_gp = gp;
                }
                sum_l = Likelyhood::zero();
            } else {
                sum_l = sum_l + l;
            }
        }
        (max_gp, max_l)
    }

    pub fn write_results_vcf<P: AsRef<Path>>(&self, path: P, gzip: bool) -> Result<()> {
        let results: Vec<_> = (if self.cfg.use_state_likelihood {
                debug!("Selecting candidates of best states");
                self.best_states()
            } else {
                debug!("Selecting best candidates");
                self.best_candidates()
            }).iter().map(|st| (*st).cand.clone() ).collect();
        write_candidates_vcf(path, &self.sampleset, &results[..], &self.unphased, gzip)
    }
}
