use pbwt::{PBWTCursor, PBWTState};
use std::collections::HashMap;
use super::super::*;
use super::*;
use ::utils::*;

#[derive(Debug, PartialEq)]//, Serialize, Deserialize)]
pub struct State {
    pub hist: GenoPair,
    pub refs: [PBWTCursor; 2],
    pub cand: GenoPair,
    pub state_l: Likelyhood,
    pub cand_l: Likelyhood,
    pub reads_poster_vec: Vec<(Likelyhood, Likelyhood)>,
}

impl State {
    pub fn new(refs: PBWTCursor) -> Self {
        State {
            hist: GenoPair::new(),
            refs: [refs.clone(), refs],
            state_l: Likelyhood::from_float(1.0),
            cand_l: Likelyhood::from_float(1.0),
            cand: GenoPair::new(),
            reads_poster_vec: Vec::new(),
        }
    }

    /// Multiplies reads' posteriori probabilities by the current variant posteriori.
    /// Sets probability to the curent variant posteriori, for reads that just started.
    pub fn update_reads_poster(&self, vp: VariantPair, read_idxs_kept: Vec<usize>,
                               readvec_curr_pos: &Vec<(String, [Likelyhood; 2], HashMap<u32, (u32, u32)>)>)
                               -> (Likelyhood, Vec<(Likelyhood, Likelyhood)>) {
        let mut reads_poster_vec_new: Vec<(Likelyhood, Likelyhood)> = Vec::new();
        let mut i: usize = 0;
        let mut readset_poster: Likelyhood = Likelyhood::from_float(1.0);
        for to_keep_idx in read_idxs_kept { // update poster. for reads that continue
            let read_post = self.reads_poster_vec[to_keep_idx];
            let read_nextvar_post = readvec_curr_pos[i].1;
            // mult. with already calculated read's poster.
            let mut read_post_updated = (read_post.0 * read_nextvar_post[vp[0] as usize],
                                                              read_post.1 * read_nextvar_post[vp[1] as usize]);

            reads_poster_vec_new.push(read_post_updated); // reads' order preserves
            // Update total poster: divide by the read's prev post. and mult. with extended by a positions
            readset_poster = readset_poster *(read_post_updated.0 + read_post_updated.1)/ (read_post.0 + read_post.1);
            i += 1;
        }
        // Insert poster. for newly added reads (start from `i`)
        for idx in i..readvec_curr_pos.len() {
            let read_nextvar_post = readvec_curr_pos[idx].1;
            // update total poster
            readset_poster = readset_poster * (read_nextvar_post[vp[0] as usize] +
                                               read_nextvar_post[vp[1] as usize]);
            // Insert the new read posteriori tuple
            reads_poster_vec_new.push((read_nextvar_post[vp[0] as usize],
                                            read_nextvar_post[vp[1] as usize]));

        }
        return (readset_poster, reads_poster_vec_new);
    }


    /// Create and push `Sate` extensions compatible with unphased `vp`.
    pub fn push_extension_with(&self, res: &mut Vec<Self>, index: &PBWTState, vp: VariantPair,
                               read_idxs_kept: Vec<usize>, normalizeBy: LogFixPoint,
                               readvec_curr_pos: &Vec<(String, [Likelyhood; 2], HashMap<u32, (u32, u32)>)>, pos: Index) {
        if vp[0] == vp[1] {
            match self.extend_val(index, vp, read_idxs_kept, normalizeBy,readvec_curr_pos, pos) { Some(x) => res.push(x), _ => () };
        } else {
            match self.extend_val(index, [0, 1], read_idxs_kept.clone(), normalizeBy, readvec_curr_pos, pos) { Some(x) => res.push(x), _ => () };
            match self.extend_val(index, [1, 0], read_idxs_kept, normalizeBy, readvec_curr_pos, pos) { Some(x) => res.push(x), _ => () };
        }
    }

    pub fn extend_val(&self, index: &PBWTState, vp: VariantPair, read_idxs_kept: Vec<usize>, normalizeBy: LogFixPoint,
                      readvec_curr_pos: &Vec<(String, [Likelyhood; 2], HashMap<u32, (u32, u32)>)>, pos: Index) -> Option<Self> {
        let refs = map01(|i| self.refs[i].extend_with(index, vp[i] != 0));
        if refs[0].len() == 0 || refs[1].len() == 0 { return None; }
        // compute multiplication of reads posteriori
        let (reads_posteriori, updated_reads_poster_vec): (Likelyhood, Vec<(Likelyhood, Likelyhood)>)  =
                                       self.update_reads_poster(vp, read_idxs_kept, readvec_curr_pos);
        let mult = Likelyhood::from_float(
            ((refs[0].len() as f64) * (refs[1].len() as f64)) /
            ((self.refs[0].len() as f64) * (self.refs[1].len() as f64)));

        Some( State {
            hist: self.hist.extend(vp),
            refs: refs,
            state_l: self.state_l * mult * reads_posteriori / normalizeBy,
            cand_l: self.cand_l * mult * reads_posteriori  / normalizeBy,
            cand: self.cand.extend(vp),
            reads_poster_vec: updated_reads_poster_vec,
        })
    }
}

impl Clone for State {
    fn clone(&self) -> Self {
        State {
            hist: self.hist.clone(),
            refs: map01(|i| self.refs[i].clone()),
            cand: self.cand.clone(),
            state_l: self.state_l,
            cand_l: self.cand_l,
            reads_poster_vec: self.reads_poster_vec.clone(),
        }
    }
}



