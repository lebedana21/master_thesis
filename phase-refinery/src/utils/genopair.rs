use std::option::Option;
use std::fmt;
use std::rc::Rc;
use std::cmp::{max, Ordering};

#[allow(unused_imports)]
use super::*;
use ::*;
use pbwt::BitVec;
use utils::{SimpleHash, map01, ChainBitVec};

/// A pair of 01 sequences. If not of equal length, they are assumed to be
/// aligned at their right ends. The missing elements are replaced by `-1` or `'*'`:
///
/// ```text
/// 00100111000
/// ******10010
/// ```
/// All indexing and iteration is from the right end, display is from the left and extend
/// is to te right. Comparison is also from the right.
///
/// Equality is based on stored 64bit hashes **only** (there may be false-positives).

//#[derive(Serialize, Deserialize)]
pub struct GenoPair {
    pub seqs: [ChainBitVec; 2],
    pub hashes: [SimpleHash; 2],
    pub full_hash: SimpleHash,
}

impl<'a> GenoPair {
    pub fn new() -> Self {
        GenoPair {
            seqs: map01(|_| ChainBitVec::new()),
            hashes: map01(|_| SimpleHash::new()),
            full_hash: SimpleHash::new().append(SimpleHash::new())
        }
    }

    #[inline]
    pub fn lengths(&self) -> [usize; 2] {
        map01(|i| self.seqs[i].len())
    }

    #[inline]
    pub fn len(&self) -> usize {
        max(self.seqs[0].len(), self.seqs[1].len())
    }

    #[inline]
    pub fn iter(&'a self) -> GenoPairIter<'a> {
        GenoPairIter { gp: self, idx: 0 }
    }

    #[inline]
    pub fn get(&self, idx: usize) -> VariantPair {
        map01(|i| if idx < self.seqs[i].len() {
                  self.seqs[i].get(self.seqs[i].len() - idx - 1) as Variant
              } else {
                  -1
              })
    }

    pub fn extend(&self, vp: VariantPair) -> Self {
        let mut s = self.clone();
        for i in 0..2 {
            s.seqs[i].push(vp[i] == 1);
            s.hashes[i] = s.hashes[i].append((vp[i] + 1) as u8);
        }
        s.full_hash = s.hashes[0].append(s.hashes[1]);
        s
    }

    pub fn erase(&self, which: [bool; 2]) -> Self {
        let mut s = GenoPair {
            seqs: map01(|i| if which[i] {
                            ChainBitVec::new()
                        } else {
                            self.seqs[i].clone()
                        }),
            hashes: map01(|i| if which[i] { SimpleHash::new() } else { self.hashes[i] }),
            full_hash: SimpleHash::new(),
        };
        s.full_hash = s.hashes[0].append(s.hashes[1]);
        s
    }

    #[inline]
    pub fn hash(&self) -> SimpleHash { self.full_hash }
}

impl PartialEq for GenoPair {
    fn eq(&self, other: &Self) -> bool {
        self.hashes == other.hashes
    }
}

impl Eq for GenoPair {}

impl Clone for GenoPair {
    fn clone(&self) -> Self {
        GenoPair {
            seqs: map01(|i| self.seqs[i].clone()),
            hashes: self.hashes,
            full_hash: self.full_hash,
        }
    }
}

impl Ord for GenoPair {
    fn cmp(&self, other: &Self) -> Ordering {
        if self == other {
            return Ordering::Equal;
        }
        for i in 0..1 << 30 {
            let si = self.get(i);
            let oi = other.get(i);
            let o = (3 * si[0] + si[1]).cmp(&(3 * oi[0] + oi[1]));
            if o != Ordering::Equal {
                return o;
            }
        }
        unreachable!();
    }
}

impl PartialOrd for GenoPair {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(Ord::cmp(self, other))
    }
}

#[derive(Clone, Debug)]
pub struct GenoPairIter<'a> {
    gp: &'a GenoPair,
    idx: usize,
}

impl<'a> Iterator for GenoPairIter<'a> {
    type Item = VariantPair;

    fn next(&mut self) -> Option<Self::Item> {
        let res = self.gp.get(self.idx);
        if res == [-1, -1] {
            None
        } else {
            self.idx += 1;
            Some(res)
        }
    }
}

impl fmt::Display for GenoPair {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.len() == 0 {
            return write!(f, "**");
        }
        for i in (0..self.len()).rev() {
            let p = self.get(i);
            for j in 0..2 {
                if p[j] == -1 {
                    write!(f, "*")?;
                } else {
                    write!(f, "{}", p[j])?;
                }
            }
            if i > 0 {
                write!(f, ",")?;
            }
        }
        Ok(())
    }
}

impl fmt::Debug for GenoPair {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "GenoPair {{ ")?;
        fmt::Display::fmt(&self, f)?;
        write!(f, " }}")?;
        Ok(())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_gp_disp() {
        let gp = GenoPair::new().extend([0, 1]).extend([1, 1]).extend([0, 0]);
        assert_eq!(format!("{}", gp), "01,11,00");
        let gp2 = gp.erase([true, false]).extend([1, 1]);
        assert_eq!(format!("{}", gp2), "*1,*1,*0,11");
    }

    #[test]
    fn test_gp_hash_eq_cmp() {
        let ht0 = GenoPair::new().extend([0, 1]);
        let ht1 = ht0.extend([1, 1]).extend([0, 0]);
        assert_ne!(ht0, ht1);
        let ht2 = ht0.extend([1, 1]).extend([0, 0]);
        assert_eq!(ht1, ht2);
        let ht3 = ht0.erase([true, false]).extend([0, 0]);
        assert_eq!(ht0.lengths(), [1, 1]);
        assert_eq!(ht3.lengths(), [1, 2]);
        assert_eq!(Ord::cmp(&ht1, &ht2), Ordering::Equal);
        assert!(ht3 < ht2);
        assert!(GenoPair::new() < ht2);
        assert!(ht0 > ht2);
    }

    #[test]
    fn test_gp_iter() {
        let ht0 = GenoPair::new()
            .extend([0, 1])
            .erase([false, true])
            .extend([1, 1])
            .extend([0, 0]);
        let mut it = ht0.iter();
        assert_eq!(it.next(), Some([0, 0]));
        assert_eq!(it.next(), Some([1, 1]));
        assert_eq!(it.next(), Some([0, -1]));
        assert_eq!(it.next(), None);
    }
}
