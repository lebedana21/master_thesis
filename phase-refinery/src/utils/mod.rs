use std::vec::Vec;
use std::fmt;

mod timeit;
pub use self::timeit::timeit;

mod stats;
pub use self::stats::WeightedStat;

mod chainbitvec;
pub use self::chainbitvec::{ChainBitVec, ChainBitVecIter};

mod simplehash;
pub use self::simplehash::SimpleHash;

mod genopair;
pub use self::genopair::{GenoPair, GenoPairIter};

#[allow(dead_code)]
pub fn ref_vec<T>(v: &Vec<T>) -> Vec<&T> {
    v.iter().map(|e| e).collect()
}

pub fn map01<F: FnMut(usize) -> T, T>(mut f: F) -> [T; 2] {
    [f(0), f(1)]
}

pub fn max2<T: PartialOrd + Copy>(val: &[T; 2]) -> T {
    if val[0] > val[1] { val[0] } else { val[1] }
}

pub fn min2<T: PartialOrd + Copy>(val: &[T; 2]) -> T {
    if val[0] < val[1] { val[0] } else { val[1] }
}

pub fn interpolate<T: ::num::Integer + Into<f64> + Copy>(pos: T, locs: &[T], vals: &[f64]) -> f64 {
    match locs.binary_search(&pos) {
        Ok(i) => vals[i],
        Err(i) => {
            if i >= vals.len() {
                vals[vals.len() - 1]
            } else if i <= 0 {
                vals[0]
            } else {
                (vals[i - 1] * (locs[i] - pos).into() + vals[i] * (pos - locs[i - 1]).into()) /
                (locs[i] - locs[i - 1]).into()
            }
        }
    }
}

#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct FilterIter<A, FA, B, FB, K>
    where A: Iterator, FA: Fn(&A::Item) -> K, B: Iterator, FB: Fn(&B::Item) -> K, K: Ord + Eq {
    it_a: A,
    it_b: B,
    key_a: FA,
    key_b: FB,
    next_a: Option<A::Item>,
    next_b: Option<B::Item>,
}

impl<A, FA, B, FB, K> Iterator for FilterIter<A, FA, B, FB, K>
    where A: Iterator, FA: Fn(&A::Item) -> K, B: Iterator, FB: Fn(&B::Item) -> K, K: Ord + Eq {

    type Item = (K, A::Item, B::Item);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.next_a.is_none() || self.next_b.is_none() { return None; }
            let ka = (&self.key_a)(self.next_a.as_ref().unwrap());
            let kb = (&self.key_b)(self.next_b.as_ref().unwrap());
            if ka < kb {
                self.next_a = self.it_a.next();
            } else if ka > kb {
                self.next_b = self.it_b.next();
            } else { // Necessarily equal
                let r = (ka, self.next_a.take().unwrap(), self.next_b.take().unwrap());
                self.next_a = self.it_a.next();
                self.next_b = self.it_b.next();
                return Some(r);
            }
        }
    }
}

pub fn filter_common_by<A, FA, B, FB, K>(mut it_a: A, key_a: FA, mut it_b: B, key_b: FB)
                                         -> FilterIter<A, FA, B, FB, K>
    where A: Iterator, FA: Fn(&A::Item) -> K, B: Iterator, FB: Fn(&B::Item) -> K, K: Ord + Eq {
        let (na, nb) = (it_a.next(), it_b.next());
        FilterIter {
            it_a: it_a,
            it_b: it_b,
            key_a: key_a,
            key_b: key_b,
            next_a: na,
            next_b: nb,
        }
}

/// Iterator returning pairs of consecutive of `Copy` item as `(current, Option<next>)`.
/// Useful for small copyable items or reference iterators.
/// Every item is returned once as `current`, the number of items stays is the same.
/// E.g. for `vec!(0,1,2,3)` the returned pairs are: `(0, Some(1)), (1, Some(2)), (2, Some(3)),
/// (3, None)`.
pub struct ConsPairsIter<T: Iterator> {
    iter: T,
    prev: Option<T::Item>
}

impl<T: Iterator> Iterator for ConsPairsIter<T> where T::Item: Copy {
    type Item = (T::Item, Option<T::Item>);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(prev) = self.prev {
            self.prev = self.iter.next();
            Some((prev, self.prev))
        } else { None }
    }
}

/// Construct a `ConsPairIter` around `it`. Warning: immeditelly calls
impl<T: Iterator> ConsPairsIter<T> where T::Item: Copy {
    pub fn new(mut it: T) -> ConsPairsIter<T> {
        let f = it.next();
        ConsPairsIter { iter: it, prev: f }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filter_common() {
        let a = vec![(2u8, 4.0), (3, 6.0), (4, 8.0), (5, 10.0)];
        let b = &[1u32..10, 2..5, 4..9];
        let comm: Vec<_> = filter_common_by(a.iter(),
                                            |x| x.0 as u16,
                                            b.iter(),
                                            |y| y.start as u16).collect();
        assert_eq!(comm, vec![(2u16, &(2u8, 4.0), &(2u32..5)), (4, &(4, 8.0), &(4..9))]);
    }
    #[test]
    fn test_interpolate() {
        let locs = vec![13, 17, 20, 120];
        let vals = vec![-1.0, -5.0, 0.0, 10.0];
        assert_eq!(interpolate(12, &locs, &vals), -1.0);
        assert_eq!(interpolate(13, &locs, &vals), -1.0);
        assert_eq!(interpolate(120, &locs, &vals), 10.0);
        assert_eq!(interpolate(121, &locs, &vals), 10.0);
        assert_eq!(interpolate(14, &locs, &vals), -2.0);
        assert_eq!(interpolate(82, &locs, &vals), 6.2);
        assert_eq!(interpolate(20, &locs, &vals), 0.0);
    }

    #[test]
    fn test_min2_max2() {
        assert_eq!(min2(&[3i8, 6]), 3);
        assert_eq!(min2(&[5i8, -1]), -1);
        assert_eq!(max2(&[3i8, 6]), 6);
        assert_eq!(max2(&[5i8, -6]), 5);
    }

    pub fn apx_eq(a: f64, b: f64) -> bool {
        (a / b) < 1.005 && (a / b) > 0.995
    }
}

