mod hash {
    pub const MODULO: u64 = (1 << 48) - 233; // Prime
    pub const MULT: u64 = (1 << 31) - 1; // Prime
    pub const INIT: u64 = 1;
}

#[derive(Eq, PartialEq, Debug, PartialOrd, Ord, Clone, Copy, Serialize, Deserialize)]
pub struct SimpleHash(u64);

impl SimpleHash {
    #[inline]
    pub fn new() -> Self { SimpleHash(hash::INIT) }

    #[inline]
    pub fn append<T: Into<u64>>(&self, v: T) -> Self {
        SimpleHash((self.0.wrapping_mul(hash::MULT).wrapping_add(v.into())) % hash::MODULO)
    }

    #[inline]
    pub fn slice<T: Into<u64> + Copy>(&self, s: &[T]) -> Self {
        s.iter().fold(*self, |h: Self, v| h.append(*v))
    }
}

impl Into<u64> for SimpleHash {
    fn into(self) -> u64 { self.0 }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash() {
        let h0 = SimpleHash::new();
        assert_eq!(h0.slice(&[] as &[u16]), h0);
        assert_eq!(h0.append(42u32).append(11u8), h0.slice(&[42u64, 11]));
        assert!(h0.slice(&[1u8, 3]) != h0.slice(&[3u16, 1]));
    }
}
    