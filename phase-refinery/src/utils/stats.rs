use std::fmt;

#[derive(Clone, Default, Debug)]
pub struct WeightedStat {
    weight: f64,
    val: f64,
    val_sq: f64,
    count: usize,
    w_log2_w: f64,
}

impl WeightedStat {
    pub fn new() -> WeightedStat { Default::default() }

    #[inline]
    pub fn add(&mut self, val: f64, weight: f64) {
        self.val += val * weight;
        self.val_sq += val * val * weight;
        self.weight += weight;
        self.count += 1;
        self.w_log2_w += weight * weight.log2();
    }

    pub fn mean(&self) -> f64 {
        assert!(self.count > 0 && self.weight > 0.0);
        self.val / self.weight
    }

    pub fn sigma(&self) -> f64 {
        assert!(self.count > 0 && self.weight > 0.0);
        (self.val_sq / self.weight - self.mean() * self.mean()).sqrt()
    }

    pub fn entropy(&self) -> f64 {
        // - H = sum (p * log p) = sum (w / W * log w / W) = 1/W * sum (w * (log w - log W)) =
        //     = 1/W (sum (w * log w) - sum (w * log W)) = 1/W * (sum (w * log w) - W * log W)
        self.weight.log2() - self.w_log2_w / self.weight
    }
}

impl fmt::Display for WeightedStat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.count > 0 {
            write!(f, "{} [+- {}, {} samples]", self.mean(), self.sigma(), self.count)
        } else {
            write!(f, "? [no samples]")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::tests::apx_eq;
    
    #[test]
    fn test_stats() {
        let mut st1 = WeightedStat::new();
        st1.add(1.0, 1.0);
        st1.add(1.0, 1.0);
        st1.add(2.0, 1.0);
        assert!(apx_eq(st1.mean(), 1.3333));
        assert!(apx_eq(st1.sigma(), 0.4714));
        assert!(apx_eq(st1.entropy(), 1.58496));
        let mut st2 = WeightedStat::new();
        st2.add(1.0, 0.1);
        st2.add(2.0, 0.05);
        assert!(apx_eq(st1.mean(), st2.mean()));
        assert!(apx_eq(st1.sigma(), st2.sigma()));
        assert!(apx_eq(st2.entropy(), 0.9183));
    }
}