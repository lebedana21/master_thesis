use stopwatch::Stopwatch;

const MIN_TIMEIT_MS: i64 = 100;

/// A simple timing of the given closure.
/// Runs increasingly more iterations of the closure to get a running time > 100 ms.
/// The inaccuracy is *at least* 1% and for very fast loops (e.g. > 10_000_000 / s),
/// the driving loop may make up a substantial part of the measured time.
pub fn timeit<F: Fn() -> ()>(name: &str, clos: F) {
    let mut iters: u64 = 1;
    debug!("Timing '{}' ...", name);
    while iters <= 1000_000_000 {
        let st = Stopwatch::start_new();
        for _i in 0u64..iters {
            clos();
        }
        let t = st.elapsed_ms();
        if t > MIN_TIMEIT_MS {
            info!("Ran {} iters of '{}' in {} ms, {} ms/iter, {:.3e} iters/s.",
                iters, name, t, t as f64 / iters as f64, iters as f64 / t as f64 * 1000.0);
            return;
        }
        iters *= 10;
    }
    info!("Ran {} iters of '{}' in < 100 ms -- block too fast to time.", iters, name);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bench_empty() {
        timeit("Empty loop", || {});
    }
}