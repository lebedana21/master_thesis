\chapter{Testing design}

\section{Testing scenarios}

We were interested in how different reads' parameters influence the phasing algorithm performance. In order to examine that, we proposed our testing pipeline as follows
\begin{itemize}
	\item First of all, we simulated reads of lengths $1k$, $5k$, and $10k$ $bp$ with uniform sequencing errors of $1\%$, $5\%$ and $10\%$ (from which $10\%$ are always INDELs) for $20$ samples. In this scenario, average sequencing depth was $15$ and $P$, the number of HMM states, was $100$. We aimed at examining how reads' parameters influence the algorithm performance. We also compared the obtained results with those measured without a read panel.
	\item We tested the algorithm on the dataset described above for $P = 1000$. Our goal was to compare the algorithm performance for number of HMM states $P = 100$ and $P = 1000$. 
	\item We simulated a dataset containing reads of length $10k$ $bp$ and sequencing error rate of 20\%. We were interested in verifying the algorithm ability to improve phasing accuracy even when given reads with extremely high sequencing error rate.
	\item We reduced average dataset coverage from $15$ to $5$  reads of length $10k$ and sequencing error rates of $10\%$ and $20\%$ to test the algorithm performance on low coverage data with high errors rates (such as reads produced with the third generation sequencing platforms).    
\end{itemize}

\section{Data}

All datasets were simulated and tested on the first $475k$ markers which constitute $\sfrac{1}{4}$ of the human \textit{chromosome 20}. The chromosome in total contains approximatively $64$ million base pairs and $1.8$ million markers.


\subsection{Data sources}

We used a phased \textit{.vcf} containing approximately $2400$ samples which were announced by \textit{phase3} of the \textit{1000 Genome project} \cite{sous_genome}. We phased the first $20$ samples available in the file and the rest was used as a reference panel. 


\subsection{Dataset simulation}


We applied our algorithm to reads created with the \textit{NEAT-genReads} sequining simulator \cite{reads_sim}. We learned a base qualities model on a real PacBio dataset first, then during the simulation we rescaled the model to obtain the desired error rate and reads length.  \\
\tab In order to simulate reads for a sample, we first created a \textit{.fa} file (see Sec. \ref{sec:data_and_formats}), containing genome sequence of the sample for each haplotype separately. This was accomplished by altering variants from a phased \textit{.vcf}, containing the sample information, into a reference sequence. Then we applied the \textit{NEAT-genReads} tool with the pretrained base error model to each of the two altered reference sequences. Once both \textit{.fq} files were created, we merge them into a single file and apply \textit{bwa} aligner tool \cite{bwa} in order to obtain the \textit{.bam} file. This file was then used to create a \textit{.var} file, an input of the phasing algorithm. \\

\section{Performance evaluation}

To evaluate the phasing algorithm performance we used a common metric called \textit{Switch Error}, which is the percentage of possible switches in haplotype orientation, used to recover the correct phase in an individual \cite{switch_error}. \\
\tab In order to get the error rate for two given \textit{.vcf} files, the first one containing the ground truth and the second containing the estimated phase information, we used \textit{gzdiff} option of \textit{vcftools} software \cite{vcf_spec}.

\subsection{Environment}

Testing was conducted on a machine with $32$ $GB$ of RAM and the Intel Core $i7-4790$ CPU (clocked at $3.6$ $GHz$) with $8$ logical cores. \textit{Nine} threads were always executed in parallel which might have influenced the tested algorithms' runtime. All the files had been stored on an external HDD. 

\section{Number of HMM states}
\label{n_hmm_states}

\begin{figure}[h]
	\includegraphics[scale=0.4]{imgs/results/hg100_together}
	\centering
	\caption{Box plots of the relation between the number of HMM states and the switch error rate and time, respectively.}
	\label{fig:hg100_together}
\end{figure}

We first tested the algorithm for a randomly selected sample to select a suitable $P$. We measure the performance for a single sample for various reads parameters (on the dataset we constructed for the first testing scenario). We varied $P$ in the set $\{100, 200, 300, 500, 700, 1000\}$. \\ 
\tab The switch error rate we measured for sample $HG00100$ for IBD length equal to $1cM$ is illustrated in Fig. \ref{fig:hg100_together} via boxplots for each value of $P$. We had noticed that error rate is not straightly proportional to $P$. No relation between $P$ and the algorithm accuracy had been observed in the results of the experiment. However, from the box plot on the right side of Fig. \ref{fig:hg100_together}, illustrating the phasing runtime, we observe a significant increase with growing $P$ (approximately linear, as was discussed in Sec. \ref{sec:com_cost}).  \\
\tab From the results of the experiment we concluded that for the selected IBD length accuracy of the algorithm does not vary significantly for the tested $P$ values. Therefore, we decided to test the algorithm for $P = 100$ and $P = 1000$. The hope is that for $P=100$ the algorithm will perform significantly faster, but for $P=1000$ we expect phasing accuracy to be slightly higher at the expense of computational efficiency. 


\chapter{Results and discussion}

\section{Results}

\subsection{Scenario 1: Impact of read panel}

First of all, we tested algorithm performance for various read lengths and error rates for average dataset of coverage $15$ and $P=100$. The obtained switch error rates are illustrated in the left side plot in Fig. \ref{fig:all_together}. A gray box represents results of phaser with no reads, colorful box-plots represent results of phasing with reads of various parameters.  We observe that the algorithm performed better with presence of reads. Importantly, it holds also for reads having sequencing error rate of $10\%$. \\
\tab Specifically, the algorithm achieved $1.4\%$ mean switch error over $20$ phased samples for phasing without reads, while for phasing with reads having $10\%$ sequencing error rate, it achieved error of $1.04\%$, $0.68\%$ and $0.53\%$ for reads of length $1k$, $5k$ and $10k$, respectively. The lowest achieved mean switch error was $0.37\%$ for reads of length $10k$ $bp$ and sequencing error rate of $1\%$, as was expected. \\
\tab From the right side plot of the Fig. \ref{fig:all_together}, illustrating CPU time, we observe that time requirements of the algorithm for phasing with reads was slightly higher. It averaged at $6.4$ minutes for phasing without a read panel, and $6.6$, $7.5$ and $7.7$ minutes for phasing with reads having $1\%$ sequencing error rate and length equal to $1k$, $5k$ and $10k$, respectively. The figure underlines that the run time is independent on sequencing error rate and near-independent on reads length.  

\begin{figure}[t]
	\includegraphics[scale=0.5]{imgs/results/all_together}
	\centering
	\caption{Box plots of the relation between the read set parameters (length and sequencing error rate) and the switch error rate and time, respectively, for the dataset with depth $15$ and for $P = 100$. }
	
	\label{fig:all_together}
\end{figure}


\subsection{Scenario 2: Number of HMM states }

We increased $P$ to $1000$ and applied the phasing algorithm to the data used in the previous scenario. Fig. \ref{fig:diff_nstates} illustrates the achieved error rates for $P = 100$ and $P = 1000$. We can see that the accuracy achieved for various reads length and sequencing error rates is practically independent on $P$, which is consistent with the results reported earlier in Sec. \ref{n_hmm_states} measured for a single sample. \\
\tab The presented results were obtained for IBD length equal to $1$$cM$ and could vary for different values of the parameter. 
\begin{figure}[t]
	\includegraphics[scale=0.45]{imgs/results/diff_nstates}
	\centering
	\caption{Box plots of the relation between the read set parameters (length and sequencing error rate) and the switch error rate and time, respectively, for the dataset with depth $15$ and for $P = 100$ and $P = 1000$.}
	\label{fig:diff_nstates}
\end{figure}
\subsection{Scenario 3: Low coverage}
\begin{figure}[t]
	\includegraphics[scale=0.5]{imgs/results/lowcov_together}
	\centering
	\caption{Box plots of the relation between the read set parameters (length and sequencing error rate) and the switch error rate and time, respectively, for the dataset with depth $5$ and $15$ and for $P = 100$.}
	\label{fig:lowcov_together}
\end{figure}
 Next, we tested how well the algorithm performs on a read set containing long reads with a low coverage. In particular, we set the dataset coverage to $5$ for reads of length $10k$. These values better correspond to standard datasets produced by third generation sequencing platforms. We used the same read panels as for the previous scenarios, only down-sampled to the desired coverage. We compared results obtained for sequencing error rates of $1\%$,  $5\%$ and $10\%$ with results obtained for down-sampled dataset with the same parameters. The results of the experiment are illustrated in Fig. \ref{fig:lowcov_together}. \\
\tab We can see from the left side plot in Fig. \ref{fig:lowcov_together} that phasing performed on the dataset with coverage $5$ results in a switch error rate higher on average than phasing on the dataset with coverage of $15$. In particular, the algorithm achieved mean switch error of $0.37\%$, $0.43\%$, $0.53\%$ for the dataset with coverage $15$ and switch error of $0.43\%$, $0.52\%$, $0.67\%$ for the dataset with coverage $5$ for sequencing error of $1\%$, $5\%$ and $10\%$, respectively and read length of $10k$ $bp$. Nevertheless, accuracy of the algorithm for the dataset with the low coverage was higher than accuracy of the algorithm when no read panel was supplied (the gray box on the left side plot), for all settings of the sequencing error rate. Importantly, this also holds for read set with high sequencing error rate of $10\%$, for which the algorithm still performs more than twice as better ($1.4\%$ vs $0.67\%$). \\
\tab Although the performance of the algorithm had slightly decreased, the run time had dropped, as it might be seen from the right side plot in Fig. \ref{fig:lowcov_together}. For the the low coverage dataset, the run time of phasing without a read panel (the gray box of the right side plot) is nearly identical to run time of the algorithm with low-coverage read panel, regardless of the sequencing error rate. In particular, it took $6.36$ minutes for phasing with no read panel and $6.9$, $6.9$, $6.95$ minutes for phasing with a read panel, for sequencing error of $1\%$, $5\%$ and $10\%$, respectively.  


\subsection{Scenario 4: High sequencing error rate}

Additionally, we simulated one more dataset, containing long reads ($10k$ bp) with sequencing error rate of 20\%. Achieved switch error rate for the dataset with coverage of $15$ and $5$ is illustrated in Fig. \ref{fig:lowcov_together}. We can see from the figure that accuracy is higher for phasing with a read set with $20\%$ error rate than accuracy obtained achieved by the phasing without a read panel. However, we can see that the resulting switch error rate dropped in comparison with the result achieved for the lower error rates.

\subsection{Read panel construction}

CPU time of the hybrid phasing algorithm reported earlier in this section does not include time required to create a $.var$ file. The reason is that the tool is independent and separate from the main algorithm cycle. Moreover, construction of a read panel for a single sample takes less than a minute and is almost independent on the reads' parameters. The time is also influenced by choice of the programming language. \\
\tab CPU time required for constructing datasets described in the first scenario with coverage of $15$ and $5$ are illustrated in Fig. \ref{fig:var_time}. Note that the time was measured when $9$ threads were running in parallel.

\begin{figure}[t]
	\includegraphics[scale=0.45]{imgs/results/var_time}
	\centering
	\caption{Box plots of the relation between the read set parameters (length and sequencing error rate) and $.var$ creation time for the dataset with depth $15$ and $5$, respectively. }
	\label{fig:var_time}
\end{figure}

\section{Discussion}
 The results presented in the previous section underline the ability of the hybrid algorithm to achieve lower switch error rate when a read panel is utilized. From the results of the experiments we conclude that a decrease in the switch error rate was achieved for all examined reads' parameters. Namely, for datasets of coverage $5$ and $15$ containing reads of length $1k$, $5k$ and $10k$ $bp$ and sequencing error rates up to $20\%$. \\
 \begin{figure}[t]
 	\includegraphics[scale=0.55]{imgs/results/se_dist}
 	\centering
 	\caption{Histogram of distances between the start and end of switch error coordinates in the genome of sample $HG00099$ made by the phasing algorithm when no read panel was given and when a dataset with reads of length $1k$, $5k$ and $10k$ and 1\% sequencing error rate was given. }
 	\label{fig:se_dist}
 \end{figure}
 \tab We concluded that reads length is the primary factor increasing the algorithm accuracy. We observed that given a dataset with low sequences error rate, the algorithm was successful at regions where variant density was higher. Specifically, it was able to increase accuracy where distance between variants was less or equal to the reads length. We illustrate the distance between start and end positions of the switch errors the algorithm made given reads of length $1k$, $5k$ and $10k$ for sample with id $HG00099$ in Fig. \ref{fig:se_dist}. \\
 \tab Surprisingly, errors the algorithm made when a read panel was supplied had a small intersection with the set of errors made by the algorithm when no read panel was given than we had expected. We observed a pattern where a group of shorter errors made by the phaser when no read panel was given was followed by a single or a smaller group of errors made when a read panel was supplied, independently on the reads' length. This underlines the different nature of errors the algorithm tends to make in those two settings. \\
 \tab We also noticed that the algorithm had a tendency to fail in variants which are INDELs. For instance, the proportion of misphased INDELs was $35\%$ over all errors for dataset with sequencing error rate of $1\%$ and $50\%$ for dataset with sequencing error rate of $20\%$ for read length of $10k$ for the sample $HG00096$. That might seem odd as INDELs constitute less then $10\%$ of all variants in the human genome. We assume it might be caused by ambiguous alignment of reads near the variants. \\
 \tab We observe from the experiments that the algorithm does not gain a substantial increase in run time in comparing with run time it requires when no read panel is supplied. Moreover, gain in the CPU time constituted only around $6.5\%$ in average of the total algorithm runtime when phasing with a read panel of coverage $5$ was performed. \\
 \vbox{ We had also examined how number the of HMM states influences the algorithm performance and observed that there was no gain in accuracy for number of states $P=100$ and $P=1000$. On the other hand, run time of the algorithm depends linearly on $P$ (as it was reported in Sec. \ref{sec:com_cost}). However, the result might be influenced by the value of the expected IBD length (not tested in this work).
}
  
 \section{Conclusion}
 
 To conclude, the hybrid phasing algorithm had demonstrated capability to gain higher phasing accuracy when a read panel is utilized in various settings. This makes the algorithm suitable for use in any pipeline where reads are available and a reference panel is present. We had demonstrated that the algorithm is highly accurate for relatively small reference panel which might be beneficent for instance for other species than humans for which there is no extensive reference panel. \\
 \tab The proposed read model is similar to SHAPEIT2 read model, except that our HMM iterates between positions, while SHAPEIT2 HMM iterates over blocks of the fixed length. This makes us assume that the accuracy of the presented algorithm is comparable or even higher than the accuracy of the SHAPEIT2 algorithm. \\
 \tab Also, since the implemented algorithm shares main features with the Eagle2 algorithm, we expect the algorithms perform similarly in terms of both accuracy and CPU time when read panel is not given. The time complexity of the presented algorithm is only linearly dependent on the read set coverage and, as it was established, the algorithm remains extremely efficient in terms of CPU time when a read panel is supplied. 

\section{Future work}

The proposed read model allowed to increase the algorithm accuracy by combining information about read alignment and read base qualities. We suppose that extension of the model by alignment qualities and prior expectations on sequencing error rate might increase the model accuracy especially when reads with high error rates are supplied. \\
\tab We also plan to test the algorithm on real datasets produced by the second and third generation sequencing platforms and compare the results with other hybrid phasing algorithms. Additionally, we suggest examining the tendency of the algorithm to misphase variants which are INDELs and proposing methods to avoid the behavior.  
 
