\chapter{Background}

With recent technological advances, enormous amounts of genotype data are being generated. However, the majority of information in these data is best exploited through
phased haplotypes, which identify the alleles that were inherited together from a single parent. \\
\tab The wide range of application of inferred haplotype phase information includes, among other, characterizing the relationship between variants and disease susceptibility \cite{ph1}, imputing low frequency variants \cite{ph2}, \cite{ph3}, modeling a population separation history \cite{ph4} and inferring recombination models \cite{ph5}. This comprehensive list makes haplotype phasing an important preliminary step in majority of genetic studies. \\
\tab The necessity of phasing is apparent as the currently available technologies do not read the genome as a whole, but break it into many short fragments, which discards desired phase information. The difficulty of the problem also stems from the diversity and complexity of life, described by the laws of genetics and inheritance. This chapter is aimed at introducing the problem essentials from biological, technological and computational points of view. 

\section{Biological background}
\subsection{Genetic variation}
There are no two humans, except for identical twins, who are genetically identical. However, on average, each human is 99.9\% similar to any other human \cite{dna_sim}. A common scenario of how the difference occurs is by propagating of a harmless mutation, which may be a silent or even a positive mutation, to the following generations. Harmful mutations are propagated in the same way but are less likely to survive natural selection. \\
\tab Specific locations of differences between individuals are classified are generally referred to as \textit{variants}. In its simplest form, a variation has several different spellings, referred to as \textit{alleles} \cite{allele}. Genetic variants are classified based on whether DNA material was substituted, gained (duplicated or inserted), lost or rearranged (inverted, translocated) \cite{variants}. The most important for us will be \textit{single-nucleotide polymorphisms} or \textit{SNPs}. Those include variations in a single nucleotide and small insertions and deletions (also called \textit{indels}) ranging from $1$ to $10 000$ base pairs (bp) \cite{idls}. Fig. \ref{snp} illustrates variants classified as SNPs. Importantly, a variant is classified as SNP when it is present to some appreciable degree within a population (e.g. over $0.1\%$).\\
\tab Presence of variations along with the extensive size of genomic data forced scientists to construct so-called \textit{reference genome}, which is an idealized assembly derived from the DNA of a number of people. In order to address a variation, scientific community compares it to the reference and announces matching with the reference or matching with an alternative of the gene in the locus.

\begin{figure}[t]
	\includegraphics[scale=1]{imgs/snps_big}
	\centering
	\caption{Example of variations which could be classified as SNPs. The SNP on the left side represents a substitution of nucleotide \textit{C} to \textit{A}; the SNP on the right side is an example of INDEL, which is an insertion of two nucleotides \textit{AA}. Note that we illustrate two single \textit{3'} to \textit{5'} strands instead of double strands for simplicity.}
	\label{snp}
\end{figure}



\subsection{Haplotype}
Humans are called diploid organism because they have two alleles at each genetic locus, with one allele inherited from each parent. Each pair of alleles represents the genotype of a specific gene. A genotype is \textit{homozygous} at a particular gene locus if both alleles are identical and is \textit{heterozygous} if the alleles differ. \\
\tab  The set of alleles inherited from a single parent is referred to as \textit{haplotype} \cite{hap1}, \cite{hap2}. Therefore, one might think of \textit{haplotype estimation} (or \textit{phasing}) to be splitting alleles observed in an organism to several groups. The number of the groups is given by the organism \textit{ploidy}, which is defined as a number of complete sets of chromosomes in a cell. In case of humans, the number is two.\\ 

\begin{figure}[h]
	\includegraphics[scale=0.7]{imgs/recomb_big2.png}
	\centering
	\caption{Illustration of a recombination event between two homologous chromosomes (one coming from mother, another from father) happening during meiosis.}
	
	\label{recomb}
\end{figure}

\subsection{Genetic recombination}
\label{sec:gen_recomb}

Each haplotype is a novel and unique combination of alleles, produced before being passed from a parent to its offspring during the process called \textit{genetic recombination}. In eukaryotic cells, this process takes place during meiosis and involves genetic information exchange between homologous chromosomes. One of the possible results of a recombination event is laid out in Fig. \ref{recomb}. Four daughter cells are produced during one meiosis (each is haploid and has a unique combination of the parental cells genome) and only one of those will be passed to the offspring. Consequently, genetic recombination is the primary process contributing to genetic variance and describing how genetic information is passed between generations. \\
\tab Therefore, we expect that related individuals share common segments of DNA. In genetic genealogy, those segments are called to be \textit{identical by descent (IBD)}. Their length depends on the number of generations since the most common ancestor at the locus of segment \cite{ibd}, due to the essence of the recombination event. Many studies conducted on population rely upon presence of the IBD patterns. \\


\subsection{Genome sequencing}
\label{sec:genome_seq}

An organism genome sequencing is performed to access its genetic information. Formally, \textit{DNA sequencing} is a process of determining the precise order of nucleotides within a DNA molecule. Currently available technologies generate a set of \textit{reads} (fragments of original DNA), instead of reading a complete genome as a whole. \\
\tab Sequencing techniques are commonly divided into generations as follows \cite{g}: 
\begin{itemize}
	\item \textit{First-generation} methods enabled sequencing of clonal DNA populations.
	\item \textit{Second-generation} methods massively increased throughput by parallelizing many chemical reactions.
	\item \textit{Third-generation} methods attempting direct DNA sequencing at the single molecule level.
\end{itemize}
\tab The first generation mainly refers to the Sanger method, which in mass production form was the technology which produced the first human genome in 2001. However, the cost and time was a major stumbling block. The short span of years starting in 2005 has marked the emergence of a new generation of sequencers to break the limitations of the first generation \cite{seq6}. Nowadays, first-generation sequencing is almost completely substituted with the second and the third-generations sequencing technologies. \\
  \tab The second-generation methods require breaking long strands of DNA into small segments. After the breaking, those fragments are amplified and synthesized \cite{illumina}. The process of synthesis is carefully monitored, for instance with a usage of fluorescent molecules, and the original DNA fragment is thereby read. Massive parallelization of the processes make the technology much faster and less costly in comparison with the first-generation technologies.  \\
  \tab Instead of breaking DNA on fragments, the third-generation sequencing works by reading the nucleotide sequences at the single molecule level. This results in substantially longer reads than methods of the previous generations were capable to produce. The difference also influences time requirements of the methods, cost, and amount of sequencing errors. \\
  \tab An overview of the main features of the popular sequencing platforms of the first, second and third generations are presented in the Tab. \ref{tab:seq_tech}. \\
   \tab While being still under active development, the third-generation technologies suffer from high error rate, as it can be observed from Tab. \ref{tab:seq_tech}. However, the methods have a great potential and are expected to bring an essential impact in the near future.
  \begin{table}[t]
  	\centering
  	\resizebox{\textwidth}{!}{%
  		\begin{tabular}{@{}lccll@{}}
  			\textbf{Platform} & \textbf{Read length (bp)*} & \textbf{Error rate (\%)*} & \textbf{Advantages} & \textbf{Disadvantages} \\ \midrule
  			\textbf{\begin{tabular}[c]{@{}l@{}}First \\ Generation  \end{tabular}} &  &  & \textit{Low error rate} & \textit{Cost, speed} \\ \midrule
  			ABI Sanger & 400-900 & 0.3 &  &  \\ \midrule
  			\textbf{\begin{tabular}[c]{@{}l@{}}Second \\ Generation  \end{tabular}} &  &  & \textit{\begin{tabular}[c]{@{}l@{}}Cost, speed\\ (massively parallel)\end{tabular}} & \textit{Short reads} \\ \midrule
  			454 & 100-700 & 1 &  &  \\ \midrule
  			Illumina & 150-300 & 0.1-1 &  &  \\ \midrule
  			SOLiD & 75 & 0.1 &  &  \\ \midrule
  			Ion Torrent & 200-400 & 1 &  &  \\ \midrule
  			\textbf{\begin{tabular}[c]{@{}l@{}}Third Generation\\ \end{tabular}} &  &  & \textit{\begin{tabular}[c]{@{}l@{}}Cost, speed, \\ availability, \\ long reads\end{tabular}} & \textit{High error rate} \\ \midrule
  			PacBio & 1.3k-15k & 12-15 &  &  \\ \midrule
  			Oxford Nanopore & 10k & 12 &  & \\ \midrule
  		\end{tabular}%
  	}
  	\caption{Main characteristics of available platforms for genomic data sequencing reported in \cite{seq6}. \newline (\textbf{\textasteriskcentered}) An interval represents range of average values producing by different instruments of the platform; a single value represents average for the platform.}
  	\label{tab:seq_tech}
  \end{table}
  

\section{Data formats}
\label{sec:data_and_formats}

An organism genome sequencing results in one or multiple \textit{.fastq} files, containing so-called raw sequencing data. It includes multiple sequences of the four nucleotides \textit{A, C, T, G} (in case of DNA sequencing) along with estimated sequencing qualities for each base encoded as single byte ASCII codes (see Tab. \ref{tab:formats} for details). \\
\begin{table}[b]
	\centering
	\label{tab:formats}
	\resizebox{\textwidth}{!}{%
		\begin{tabular}{|c|l|}
			\hline 
			Format   & \multicolumn{1}{|c|}{Content}                                                                                                                                                                   \\ \hline
			.fasta    & \begin{tabular}[c]{@{}l@{}}$\bullet$ header starting with '\textgreater', containing a sequence name \\ \tab $\bullet$ \textit{>reference\_chr20}\\ $\bullet$ sequence \\ \tab $\bullet$ \textit{ATAGACATAGA..} \end{tabular}                                                                                                       \\ \hline
			.fastq    & \begin{tabular}[c]{@{}l@{}} \textbf{For each read contains:} \\ $\bullet$ name \\ \tab $\bullet$ \textit{@read1} \\ $\bullet$ sequence \\ \tab $\bullet$ \textit{ATAGACATAGA..} \\ $\bullet$ base qualities (encoded as single byte ASCII codes) \\ \tab \textit{$\bullet$ $!*((((***+)..$} \end{tabular}                                                                                                                                       \\ \hline \href{https://samtools.github.io/hts-specs/SAMv1.pdf}{.sam/.bam}
			& \begin{tabular}[c]{@{}l@{}} $\bullet$ header lines starting with '@' \\ \tab  $\bullet$ format version \\ \tab  $\bullet$ sort order of alignments etc. \\  \textbf{For each read contains:} \\ $\bullet$ information about the alignment \\ \tab  $\bullet$ read' position \\ \tab  $\bullet$ mapping quality \\ \tab  $\bullet$ FLAG - set of additional information describing the alignment \\ \tab  $\bullet$ TAGs - additional optional information\\ $\bullet$ mate pair/paired end reads information\\ \tab  $\bullet$ mate position \\ \tab  $\bullet$ inner size etc.\\ $\bullet$ quality and alignemnt denoted by mapping/pairing \\ \tab  $\bullet$ cigar - reference to read subsequence and associated mapping operation (matched, clipped etc.) \\ $\bullet$ read's information \\ \tab  $\bullet$ name  \\ \tab  $\bullet$ sequence \\ \tab  $\bullet$ bases' qualities as Phred Quality Scores
			\end{tabular}      \\ \hline \href{https://samtools.github.io/hts-specs/VCFv4.2.pdf}{.vcf/.bcf}
			& \begin{tabular}[c]{@{}l@{}}$\bullet$ meta-information in a form \textit{\#\#key=value}, where \\ \tab $\bullet$  key is e.g.  \textit{INFO, FILTER, FORMAT} or other \\ $\bullet$ header naming 8 mandatory columns: \textit{\#CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO} \\ \textbf{For each called variant contains:} \\ $\bullet$ variant describing fields, which correspond to mandatory header columns \\ \tab $\bullet$  \textit{20, 14370, rs6054257, G, A, 29, PASS, NS=3; DP=14; AF=0.5} \\ $\bullet$ genotype fields for each present samples, which correspond to header optional field \textit{FORMAT}. When \\ \textit{FORMAT} is not specified, the field contains genetic information only \\ \tab $\bullet$ 0|1 - in case of phased variant \\ \tab $\bullet$ 0/1 - in case of unphased variant \\
			\end{tabular}   \\  \hline 
		\end{tabular}%
	}
\caption{A brief description to the most common bioinformatics file formats. }
\end{table}
\tab In order to operate with the information, one is required to align the obtained reads to a reference sequence, which is commonly stored in \textit{.fasta} format (see Tab. \ref{tab:formats}). The alignment results in a file in \textit{.sam} format, which contains estimated positions of the reads in the original genome and the alignment qualities for each read's base. Commonly, the next step in genetics studies is variant calling, which result in a \textit{.vcf} file. This allows to identify SNPs and other larger polymorphisms, that the sequenced genome possesses. \\
\tab Among other, a \textit{.vcf} file contains information specifying weather observed genotype is equal to the reference allele (denoted as \textit{zero}) or to the alternative allele (denoted as \textit{one}), or both, for each listed position for each present sample. Genotype containing both \textit{zero} and \textit{one} in a marker would mean that the sample is heterozygous in the variant. It might also happen that more than two alleles would be observed in a population at a specific marker. In this case, \textit{.vcf} would contain a list of the alternatives listed in \textit{ALT} column (see Tab. \ref{tab:formats} for details). In the case of a multialelic site integers are used to refer to one of the alleles (corresponding to the allele position in the list), analogically to \textit{one}, used to denote an alternative in case of a bialelic site.  \\
\tab There are plenty of other bioinformatics data formats, e.g. \textit{CRAM, ORC, Avro } etc. The mentioned ones are those considered the most common and those which we primary access during the study. In further text, we describe one more data format which we designed in order to optimally utilize haplotype information preserved in reads which span more than one variant. Details of the format are present in Sec. \ref{sec:var}.

\chapter{Problem statement}

\section{Phasing problem}
\label{intro:phasing_problem}

We define phasing as a process of haplotype estimation from genomic data. Currently available sequencing technologies do not allow to gain haplotype information directly from a biological experiment with desirable efficiency, as it was discussed in the previous section. For the reason various computational methods are applied to estimate haplotypes (e.g. \cite{phase_method}, \cite{phase_method2}, \cite{eagle}, \cite{beagle} etc.). \\
\tab Generally, the input of a phasing software is a \textit{.vcf} file, with contains information about alleles the phasing sample has at each polymorphic position along the genome. However, mutual information about the markers' states is lost. Therefore, a phasing software aims at determining which alleles appear together on the same chromosome, i.e. estimating haplotype information. Henceforth we consider a human diploid genome and refer to the two haplotypes as \textit{maternal haplotype} and \textit{paternal haplotype}, we denote as $h^{\alpha}$ and $h^{\beta}$ respectively.\\


\subsection{The importance of phase information}

The importance of phase information for human genomics might not be obvious. However, many findings — both from recent studies and in the more established medical genetics literature — indicate that relationships between genotype and phenotype, including genetic disease, can be better understood with phase information \cite{ph1}. \\
\tab For instance, there are clinical conditions and disorders influenced by compound heterozygosity within a single genes, e.g. \textit{Cerebral Palsy}, \textit{Hemachromatosis}, \textit{Mediterranean Fever} and many others \cite{ph1}. Since most of mutations are inherited from parents, sequencing of the parental genomes might be performed to estimate phase information. However, it is never guaranteed that phase information inferred from parental genomes will be univocal. For instance, in case when both parents are heterozygous in markers of interest, it is not clear which alleles were inherited together. Meanwhile, computationally methods for haplotype estimation allow diagnosing the conditions with no need to sequence parental genomes. \\ 
\tab Phase information is also important in the population studies. For instance, it was demonstrated in \cite{import_phasing} that greater differentiation of human populations can be obtained by exploring within- and across-population haplotype diversity than by focusing on multilocus genotype diversity. \\
\tab Finally, phase information is greatly used in order to model recombination points and identify variants linkage and interactions among multiple genes and alleles.

 
\begin{figure}[t]
	\includegraphics[scale=0.9]{imgs/haplotype_big_new}
	\centering
	\caption{Toy example illustrating phasing problem input and output. \newline (\textbf{\textasteriskcentered}) Note that we do not actually know which hapotype came from the father and which from the mother, but we know that each came from a different parents. That is why alleles of the first ambiguous variant is randomly assigned to one of the haplotypes. }
	\label{fig:gen_hap} 
\end{figure}

\subsection{Ambiguity and Data sources}
 
 To understand the problem, let us consider a toy \textit{.vcf} illustrated in Fig. \ref{fig:gen_hap}. It contains five variants only, three of which are heterozygous (those are highlighted in red). Phase information in those is uncertain and the rest is identical for both estimated haplotypes. \\
 \tab The tree illustrated in Fig. \ref{fig:gen_hap} represents phasing problem solution space.
 Here, each path from the tree root to a leaf represent a consistent with the observed genotype solution. Considering a general task of haplotype estimation for $n$ heterogeneous markers, there are $2^{n-1}$ consistent possibilities. Even so, naturally, only one of the pairs corresponds to the true haplotypes.\\
 \tab There are two additional information sources, allowing to estimate a diplotype correctly and to avoid ambiguity. We refer to the sources as \textit{Reference Panel} and \textit{Read panel}.

 
 \subsubsection*{Reference panel}
  Reference panel is an extensive high-quality source of genomic data for multiple samples stored in \textit{.vcf} format. Construction of the reference panel is often made on near-close population, i.e. of European ancestry. This ensures presence of IBD patterns (see Sec. \ref{sec:gen_recomb}) and increases the patterns' length.  \\
 \tab The largest currently available reference panel contains haplotypes of 64,976 humans at 39,235,157 SNPs \cite{ref_panel}. A very complex and expensive pipeline was followed to obtain the desired high-quality data for so many individuals \cite{ref_panel}. The results provide a foundation for population-based cohort studies such as estimation of distribution, risk factor analysis, recombination model estimation etc. \cite{popbased_stud}.

	\begin{figure}[t]
		\includegraphics[scale=0.9]{imgs/reads_big_new}
		\centering
		\caption{Toy example illustrating how phase information is preserved in reads.}
		\label{fig:reads} 
	\end{figure}
	
 
 \subsubsection*{Read panel}
 
 \label{sec:read_panel}
  Read panel is an abstract term used to refer to the set of reads produced by an individual's genome sequencing and alignment. It is significant that a read panel comprises information about the individual genotype, as well as information about nearby alleles' relation. \\
 \tab Available sequencing techniques, presented in Sec. \ref{sec:genome_seq}, ensure that each produced read was entirely sampled from a single haplotype. Therefore, variants which a read covers also have the property. \\
 \tab For instance, let us consider an example introduced previously, but enhanced with aligned reads. Fig. \ref{fig:reads} illustrates the example. We observe that reads which cover more than one variant allow to partially or completely resolve the ambiguity and to restore the original haplotypes. \\
 \tab Imagine an extreme example, where two reads are present and their lengths are equal to genotype size. Original haplotypes would simply correspond to the reads and phasing would be done in one step. \\
 \tab It was mentioned in Sec. \ref{sec:genome_seq} that no reliable technology able to do molecular sequencing is currently available. Nevertheless, latest sequencing technologies, in particular technologies of the third generation, produce reads of extensive length, covering many consecutive variants. In fact, any read covering more than one variant might be beneficially utilized during phasing. 

 

\subsubsection*{Genetic map}
\label{sec:gmap}
Another source of information, which might be utilized during phasing, is a genetic map. It contains statistics about propensities towards crossover between chromosome positions measured in centimorgans. Formally, a centimorgan is defined as the distance between chromosome positions for which the expected average number of crossovers in a single generation is 1\%. Roughly speaking, one centimorgan corresponds to about $one$ million base pairs in humans on average. However, the relationship varies from place to place in the genome, and also between men and women.\\
\tab In particular, population-based phasing methods use a genetic map to model recombination event probability distribution. \\

\section{Related works}

There are plenty of haplotype estimation algorithms, differing in initial assumptions about the input data. Haplotype phasing becomes straightforward if the family genetic information is available or if molecular sequencing is made. However, generally, we do not assume that is the case and rely on other available data sources such as reference panel or read panel, introduced earlier. In general, we also assume that the individual is not directly related to samples in reference panel. \\
\tab Considering the assumptions listed above, we distinguish between three families of phasing methods, namely population-based phasing, read based-phasing and a hybrid of the two approaches.  

\subsection{Population based phasing}

Haplotypes can be inferred from genotype information of large cohorts based on the presence of IBD patterns, which holds for related as well as unrelated individuals as is reviewed in \cite{hap_methods}. This family of methods is referred to as \textit{population-based phasing} and is based on phased genetic information of a big enough number of individuals, i.e. reference panel. The fact that an unknown haplotype is derived from known haplotypes by mutation and recombination processes is incorporated in many population-based methods, including PHASE \cite{phase_method}, \cite{phase_method2}, its faster implementation SHAPE-IT \cite{shapeit}, FastPHASE \cite{phase_method2}, Beagle \cite{beagle}, Eagle and Eagle2 \cite{eagle}, achieving greater accuracy in comparison with Eagle when reference panel size decreases. \\ 
\tab Each of the listed algorithms uses hidden Markov model (HMM) whose parameters are estimated with the use of iterative algorithms such as the stochastic EM algorithm \cite{hap_methods}. Nevertheless, Eagle2 is capable of achieving high accuracy as well as essential speed-up \cite{eagle} in comparison with other algorithms even for small reference panels. There are two key ideas distinguishing Eagle2 from existing population-based methods: a new data structure based on the positional Burrows-Wheeler transform \cite{pbwt} and a rapid search algorithm that explores only the most relevant phase paths through the HMM \cite{eagle}. This difference made Eagle2 the state-of-art algorithm for population-based phasing. \\
\tab Currently available methods for population-based phasing allow to perform haplotype estimation accurately and in acceptable time. However, information present in read panel, which is naturally available after genome sequencing, is not used up by the methods.    

\subsection{Read-based phasing}

Recent achievements in sequencing technologies, associated especially with the third generation sequencing (see Sec. \ref{sec:genome_seq}), allow reconstructing haplotype from reads' information. As it was discussed in Sec. \ref{sec:read_panel}, read panel provides information that can be incorporated into computational phasing \cite{hap_methods}. The longer the sequenced fragment, the greater amount of useful information it carries. Building upon the assumptions, \textit{read-based phasing} techniques are being actively developed.  \\
\tab While in 2011 only few methods were available, nowadays there is plenty of algorithms including HapCUT (2008) \cite{rb:hapcut}, RefHap (2010) \cite{rb:refhap}, Read-backed phasing (2012), H-BOP (2012) \cite{rb:hbop}, ProbHap (2014) \cite{rb:probhap}, What'sHap (2015) \cite{rb:whap} and HapCol (2016) \cite{rb:hapcol}. While being different in methodology, the algorithms also differ in assumptions about the input data, e.g. reads length, sequencing coverage and error rate. \\
\tab Earlier methods, such as HapCUT, are designed for reads generated with first generation sequencing, mainly Sanger sequencing (low error rate, relatively long reads, $>700bp$ \cite{seq:sang}). This method does not outperform currently available methods in terms of accuracy and is slower in general settings \cite{rb:whap}. Moreover, first generation sequencing is too expensive for whole-genome sequencing on a large scale and is gradually being substituted with the second and the third generations techniques, as it was described in Sec. \ref{sec:genome_seq}. \\ 
\tab Majority of methods listed above formalize read-based phasing (also called haplotype assembly) as Minimum Error Correction problem (MEC) or its variation weighted MEC (wMEC), where given a set of fragments, the goal is to reconstruct two haplotypes by applying the minimum number of base corrections \cite{mec}. The problem is NP-hard \cite{mec}, but the methods use approximation (RefHap, HapCUT, H-BOP) as well as fix-parameter approaches (What'sHap, HapCol, ProbHap), where the parameter is sequencing depth. Apparently, approximations are faster in general but do not give guarantees on solution quality, while fix-parameter approaches are exact but their time requirements grow exponentially with the growth of the input parameter value. \\
\tab The latest ones What'sHap and HapCol are mainly designed for the third generation sequencing data. The methods are appropriate for the datasets since What'sHap complexity is not affected by reads' length, while HapCol run time grows only linearly with the length. Although the algorithms run time increases exponentially with increasing read set coverage, authors of What'sHap claim that coverage greater than 20 nearly does not influence result quality, assuming reasonable filtering of input fragments. \\
\tab All listed methods require some guarantees on input parameters, i.e. sequencing depth, reads' length, error rate, and are not capable to generalize on the input. While third generation sequencing technologies allow retrieving majority of haplotype solely based on reads, they still suffer from high error rate (see Tab. \ref{tab:seq_tech}). Importantly, the performance of the algorithm, primarily designed for the technology, decreases rapidly with increase sequencing error rate \cite{rb:whap}, \cite{rb:hapcol}, which makes the methods inefficient for datasets currently produced by the third generation platforms.

\subsection{Hybrid phasing}
Hybrid phasing utilizes availability of both a reference panel and a read panel. The hybridization often allows reaching better algorithm performance in comparing with solo approaches. \\
\tab  There are several available hybrid algorithms, including  SHAPEIT2 (2013) \cite{shapeit2}, Hap-seq (2013) \cite{hapseq} and PhASER (2016) \cite{phaser}. Hap-seq represents an exact phasing algorithm which is exponential in the number of variants a read covers. Authors fix the number to avoid the exponential complexity, which leads to suboptimal usage of information a read panel supplies. The algorithm is similar to SHAPEIT2, but instead of Markov chain Monte Carlo (MCMC) sampling it uses the exact Viterbi algorithm. \\
\tab SHAPEIT2 algorithm extends population-based phasing algorithm SHAPEIT by likelihoods computed under the read panel. This extension, among other, allows phasing variants, that are singletons and not present in reference panel \cite{shapeit2}. The algorithm, similarly to Hap-seq, iterates by segments of fixed size (from 0.1Mbp to 5Mbp). Differently from the Hap-seq algorithm, the model counts with reads which span the segments' boundaries. The algorithm outperforms SHAPEIT as well as Beagle \cite{beagle} population-based phasing algorithm in terms of phasing accuracy \cite{shapeit2}. Accuracy of SHAPEIT2 algorithm increases with increasing reads' length. This property makes the algorithm more suitable for the third generation sequencing technologies. However, it is reported in \cite{shapeit2} that when paired-end short reads were supplied (similar to the output of the second generation sequencing platform $Illumina$), increase in the accuracy was observed as well. In particular, for variants that are not in the reference panel, the switch error dropped from $49.3\%$ to $37.1\%$ and at sites with a minor allele count
of $eight$ the switch error dropped from $2.2\%$ to $1.7\%$. This underlines the ability of the short reads to increase phasing accuracy. \\
\tab The newest algorithm, PhASER, is primarily designed for RNA-seq. The algorithm, similarly to other presented hybrid approaches, iterates over blocks (up to 15 variants) and extensively searches the solution space. PhASER constructs a solution space basing on a read panel and utilizes reference panel to compute haplotypes likelihoods. Run time and accuracy of the algorithm for WGS datasets are similar to performance of the HapCUT population-based phasing algorithm \cite{rb:hapcut}, introduced earlier \cite{phaser}. \\
\tab To best of authors knowledge, there is only one hybrid algorithm SHAPEIT2, which combines both read panel and reference panel into a single probabilistic model, while fully utilizing phase information contained in reads. It achieves better performance than it's population based phasing counterpart, underlining ability of read panel to improve population-based phasing. Its time complexity is similar to SHAPEIT population-based phasing algorithm.

\section{Objectives}

While currently available read-based phasing algorithms utilize the ability of long reads to span more variants, they are inefficient for short reads produced by the first and the second sequencing technologies, as was reported in e.g. \cite{rb:whap}. However, high sequencing error rate the long reads suffer from makes the algorithms inefficient for low coverage data. For instance, What's Hap algorithm for PacBio datasets achieves switch error around $10\%$ while phasing both substitutions and INDELs for simulated data and error rate around $15\%$ on real PacBio dataset with coverage $5$, as is reported in \cite{rb:whap}. Increasing the coverage to $60$ reduces the errors to $3.8\%$ for simulated dataset and to $8.3\%$ for real dataset. However, in general datasets does not have such depth due to cost of such sequencing and time demands. Moreover, the most efficient read-based phasers (e.g. \cite{rb:whap}, \cite{rb:hapcol}, \cite{rb:probhap}) are exponential in read dataset coverage. \\
 \tab Meanwhile, population-based phasing algorithm are very expensive in term of run time, except Eagle2 algorithm. For instance, in \cite{eagle} it is reported that Eagle2  achieves $12$–$38$x speedups over SHAPEIT in performance on a reference panels of size $15,000$–$100,000$.\\
 \tab Therefore, the goal of our research is to extend Eagle2 phasing algorithm \cite{eagle}, current the state-of-art population-based phasing algorithm, with information preserved in reads. The information is nearly always available since genome sequencing is the first step performed in any genome analysis pipeline. The presented algorithm aimed at being efficient in term of run time and phasing accuracy for reads obtained with variant available sequencing technologies. 

\section{Student contribution}
The student contribution was reviewing the existing phasing methods and design of the Eagle2 algorithm extension with read-based phasing. This includes proposal of the probabilistic model, implementation of the read-based phasing part of the algorithm (including implementation of the tool for reads preprocessing and conversion into the desired format), data simulation and testing of the hybrid algorithm. 

\section{Outline}

The thesis is structured as follows. In the previous two chapters, we introduced basic terms, the phasing problem itself and listed currently available phasing algorithms. The following chapter introduces the proposed hybrid phasing algorithm. In the chapter, we formally describe the proposed probabilistic model and its approximation with HMM. In chapter \ref{ch:impl} we describe implementation details of the algorithm. The last chapter contains testing results and a discussion about them.
