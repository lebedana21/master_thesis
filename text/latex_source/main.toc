\select@language {english}
\select@language {english}
\contentsline {part}{I\hspace {1em}Introduction}{1}{part.1}
\contentsline {chapter}{\numberline {1}Background}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Biological background}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Genetic variation}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Haplotype}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Genetic recombination}{4}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Genome sequencing}{5}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Data formats}{6}{section.1.2}
\contentsline {chapter}{\numberline {2}Problem statement}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Phasing problem}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}The importance of phase information}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Ambiguity and Data sources}{10}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Related works}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Population based phasing}{12}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Read-based phasing}{13}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Hybrid phasing}{13}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Objectives}{14}{section.2.3}
\contentsline {section}{\numberline {2.4}Student contribution}{15}{section.2.4}
\contentsline {section}{\numberline {2.5}Outline}{15}{section.2.5}
\contentsline {part}{II\hspace {1em}Materials and Methods}{17}{part.2}
\contentsline {chapter}{\numberline {3}Eagle2 phasing algorithm}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Algorithm overview}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Step 1: HapHedge data structure construction}{19}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Step 2: Exploration of diplotype space}{20}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Proposed extension}{20}{section.3.2}
\contentsline {chapter}{\numberline {4}Hybrid phasing algorithm}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}Preliminaries}{21}{section.4.1}
\contentsline {section}{\numberline {4.2}Diplotype probability model}{21}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Haplotype probability model}{22}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Read model}{22}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Haplotype inference}{23}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Hidden Markov model}{23}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}State space exploration}{23}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Implementation}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Tool for Read Panel Construction}{25}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}The .var format}{26}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Execution}{26}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Phasing Algorithm}{28}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Step 1: Read panel construction}{28}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Step 2: PBWT Index construction}{28}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Step 3: Haplotype inference}{28}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Computational cost}{29}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Discussion}{30}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Execution}{31}{section.5.4}
\contentsline {part}{III\hspace {1em}Results}{33}{part.3}
\contentsline {chapter}{\numberline {6}Testing design}{35}{chapter.6}
\contentsline {section}{\numberline {6.1}Testing scenarios}{35}{section.6.1}
\contentsline {section}{\numberline {6.2}Data}{35}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Data sources}{35}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Dataset simulation}{36}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Performance evaluation}{36}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Environment}{36}{subsection.6.3.1}
\contentsline {section}{\numberline {6.4}Number of HMM states}{36}{section.6.4}
\contentsline {chapter}{\numberline {7}Results and discussion}{39}{chapter.7}
\contentsline {section}{\numberline {7.1}Results}{39}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Scenario 1: Impact of read panel}{39}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Scenario 2: Number of HMM states }{39}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Scenario 3: Low coverage}{40}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}Scenario 4: High sequencing error rate}{41}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}Read panel construction}{41}{subsection.7.1.5}
\contentsline {section}{\numberline {7.2}Discussion}{42}{section.7.2}
\contentsline {section}{\numberline {7.3}Conclusion}{44}{section.7.3}
\contentsline {section}{\numberline {7.4}Future work}{45}{section.7.4}
