\chapter{Eagle2 phasing algorithm}
\label{ch:eagle}
The proposed phasing algorithm performs diplotype inference under the proposed HMM model. The core idea of the method corresponds to Eagle2, which is currently the state-of-art population-based phasing algorithm. The proposed algorithm extends the probabilistic model of Eagle2 by posteriori probabilities computed under a read panel. \\
\tab In this chapter we briefly describe the Eagle2 algorithm and scope of the proposed extensions. The formal description of the proposed probabilistic model and the haplotype inference procedure, performed under the model, is introduced in the following chapter. 

\section{Algorithm overview}

A statistical model of the Eagle2 algorithm is a haplotype copying model similar to the one introduced by Li N. and Stephens M. in 2003 \cite{listephens}. The Li and Stephens framework accounts for the biological processes of recombination and mutation to estimate an individual diplotype from known haplotypes of other individuals \cite{hap_methods}, i.e. reference panel. The model forms a base of probabilistic models of the majority of population-based phasing algorithms, e.g. PHASE \cite{phase_method}, SHEPEIT \cite{shapeit}, Beagle \cite{beagle}. \\
\tab However, Eagle2 differs from the dynamic programming or sampling methods employed by previous phasing software. Before haplotype space exploration, the algorithm creates a HapHedge structure, allowing efficiently representing haplotypes from reference panel. Furthermore, Eagle2 selectively explores the diplotype space. We give a brief description of the main algorithm steps. 

\subsection{Step 1: HapHedge data structure construction}
\label{sec:haphedge}
A HapHedge structure encodes a sequence of haplotype prefix trees rooted at a sequence of starting positions along the chromosome. This enables a near constant-time retrieval of haplotype frequencies at any position along the chromosome. The property makes phasing algorithm speed nearly independent on reference panel size. Moreover, the structure construction has linear-time complexity and near constant-time querying. \\
\tab The HapHedge structure is generated using the positional Burrows-Wheeler transform \cite{pbwt}. In its simplest form, the PBWT iteratively creates sorted lists of haplotype prefixes, moving the prefix start point from right to left \cite{eagle}. Eagle algorithm extends this procedure to convert the lists of sorted prefixes into prefix trees \cite{eagle}. The procedure takes $\mathcal{O}(M*K)$ time, where $K$ is the number of individuals in reference panel, $M$ is the number of variants to phase. After the structure is created, the algorithm starts to explore the diplotype space. 

\subsection{Step 2: Exploration of diplotype space}
\label{eagle:step2}
The algorithm performs a solution space exploration moving from left to right along the chromosome. At each target marker, the algorithm considers two possible extensions of the haplotype pair as well as possible recombination on maternal, paternal or both haplotypes. State likelihoods are then computed under the reference panel (by querying the HapHedge structure constructed in the previous step) and under the given unphased genotype of the sample. 

\subsubsection*{States merging}

Apparently, the number of haplotypes in the reference panel which are consistent with the haplotype will continually decrease when moving from left to right along the chromosome. This reduces the ability of the algorithm to overcome local maxima. The behavior is prevented so that states, for which haplotypes agree in the last $64$ heterozygous positions, are merged into a single state and the new state likelihood is approximated by a weighted combination of the merged states likelihoods (for more details see \cite{eagle} Sec. 2.3.2). The step also reduces the number of states, resulting in a more tractable HMM.

\subsubsection*{Space pruning}

To prevent the exponential explosion of the solution space, along with states merging, space pruning is performed. The solution space is pruned to the $P$ most likely solutions in each step. The hope is that $P$ is set to be big enough to be robust to local maxima \cite{eagle}. \\
\tab Therefore, the algorithm performs beam search of a small subset of the solution space, which is advantageous when most of the space is probabilistically unfavorable but difficult to discard a priori \cite{eagle}.
  
\section{Proposed extension}

 Newly, we add read set posteriori probabilities to the Eagle2 probabilistic model. In the proposed algorithm, we construct an efficient reference panel representation (different from HapHedge structure) using PBWT at first. Then, we precompute a posteriori probability of each read alignment contained in a supplied \textit{.bam} file, given alleles from a supplied \textit{.vcf} file. Further, the probabilities are incorporated into the probabilistic model, which takes into account both sources of information: the created read panel and the supplied reference panel. Then we, similarly to Eagle2, approximate the proposed model with HMM and iterate over it using Viterbi Algorithm, performing states merging and pruning at each step of the inference.  The proposed probabilistic model and the inference procedure are formally described in detail in the following chapter. 

\chapter{Hybrid phasing algorithm}
\label{HMM}
The following chapter contains a formal description of the proposed probabilistic model as well as a diplotype inference performed under the model. 

\section{Preliminaries}

The following hybrid algorithm is based on two sources of information, read panel $R$ and reference panel $H$. Let $N$ be the total number of variants (or markers) in a considered region of a genome. We further refer to reads, covering a position $p$ as $R_{p}$, where $p \in <1,N>$. \\
\tab We represent a diplotype as a pair of maternal and paternal haplotypes denoted as $h^\alpha$ and $h^\beta$ respectively. We refer to the value of a haplotype at a specific position $p \in <1,N>$ as $h_{p}$. We consider both heterozygous and homozygous variants since homozygous are important for inference under \textit{H}. 


\section{Diplotype probability model}

Our goal is to model a distribution of all possible diplotypes, given the available information from reads and taking into account population haplotype structure, i.e. $\textbf{p}(h^\alpha, h^\beta | R, H)$. We also take into account recombinations and a possibility of discrepancies between the sources. \\
\tab Assuming independence of $R$ and $H$ given a pair of haplotypes $h^\alpha, h^\beta$ (similarly to \cite{shapeit2}) we rewrite the probability $\textbf{p}(h^\alpha, h^\beta | R, H)$ as follows\\
\begin{equation}
\begin{gathered}
\textbf{p}(h^\alpha, h^\beta | R, H) \propto
\textbf{p}(h^\alpha, h^\beta, R, H) = \\
 \textbf{p}(H, h^\alpha, h^\beta) \cdot  \textbf{p}(R| h^\alpha, h^\beta, H) \propto \\
 \textbf{p}(h^\alpha, h^\beta | H) \cdot  \textbf{p}(R| h^\alpha, h^\beta)\\
\end{gathered} \label{dm:model}
\end{equation}
This allows us to divide our model into two independent parts, referred to as the \textit{haplotype model} and the \textit{read model}.


\subsection{Haplotype probability model}
\label{haplotype_model}
We first approximate conditional diplotype distribution given $H$ by making independence assumption on maternal and paternal haplotypes given H (similarly to \cite{eagle}), where the haplotypes are coming from the same distribution on $h_{1:N}$, i.e. 
\begin{equation}
\begin{gathered} 
	\textbf{p}(h^{\alpha}_{1:N}, h^{\beta}_{1:N}| H) \approx \textbf{p}(h^{\alpha}_{1:N}|H)\textbf{p}(h^{\beta}_{1:N}|H)
\end{gathered} \label{hm:hap_indep}
\end{equation}
\tab Next, we model each haplotype as a mosaic of recombined haplotype segments from $h_{1:a}, h_{a+1:b}, \ldots h_{l+1:N}$, copied from haplotypes from $H$. Here, we denote a set of recombination breakpoints for a haplotype as $a,b, \ldots l$, and it holds that $1 < a < b < c \ldots l< N$. We can thus decompose a probability of haplotype as a sum over the possible recombination breakpoints as 

\begin{equation}
\begin{gathered}
\textbf{p}(h_{1:M}| H) =\sum_{a,b,\ldots l}^{}  \textbf{f}(h_{1:a} | H)\textbf{p}_{rec}(a) \\
\textbf{f}(h_{a+1:b} | H)\textbf{p}_{rec}(b) \ldots \textbf{f}(h_{l+1:M} | H) \textbf{p}_{rec}(M),
\end{gathered} \label{hm:model}
\end{equation}

\noindent
where $\textbf{p}_{rec}(p), p \in <1,N>$ models the probability of a breakpoint in position $p$ and $\textbf{f}(h_{a:b} | H)$ denotes the frequency of a haplotype segment $h_{a:b}$ in $H$, i.e. 
\begin{equation}
\begin{gathered}
\textbf{f}(h_{a:b} | H) = \frac{\{k: h^k_{a:b}== h_{a:b}\}}{K},
\end{gathered} \label{hm:frac}
\end{equation}

\noindent
where $K$ denotes the reference panel size, i.e. $K=|H|$. 

\subsubsection*{Recombination probability model}
We use Li-Stephens model \cite{listephens} to describe probability $\textbf{p}_{rec}(p), p \in <1,M>$ that a recombination event occurs at the marker $p$. Under this model the probability is computed as 
\begin{equation}
\begin{gathered}
\textbf{p}_{rec}(p) = 1 - e^{-\lambda x} = 1 - e^{-\lambda (G(p) - G(p_{prev}))},
\end{gathered} \label{hm:prec}
\end{equation}

\noindent
where $\lambda$ is the expected IBD length and $G(p)$ denotes the genetic distance of marker $p$ from the beginning of the chromosome, which can be found in genetic map (see Sec. \ref{sec:gmap} for details). The parameter $\lambda$ corresponds to theoretical distance of a phased sample to samples from the reference panel $H$. And as it was shown in \cite{eagle}, phasing accuracy appears to be insensitive to $\lambda$ from the range $0.5cM-4cM$. 


\subsection{Read model}

Similarly to SHAPEIT2 \cite{shapeit2}, we compute the posteriori probability of $R$ given a pair of haplotypes as follows 
\begin{equation}
\begin{gathered} 
\textbf{p}(R| h^\alpha, h^\beta) = 
\prod_{r \in R}^{} \textbf{p}(r| h^\alpha, h^\beta)
\end{gathered} \label{rm:read_set}
\end{equation}
Assuming that prior probabilities of the haplotypes being sequenced are equal, we rewrite the equation as follows
\begin{equation}
\begin{gathered}
\prod_{r \in R}^{} \textbf{p}(r| h^\alpha, h^\beta) = 
\prod_{r \in R}^{} \frac{\textbf{p}(r| h^\alpha) + \textbf{p}(r| h^\beta)}{2}
\end{gathered} \label{rm:sum}
\end{equation}
Assuming independence of bases within a read, we utilize read base qualities, available after sequencing to rewrite posteriori of a read as follows
\begin{equation}
\textbf{p}(r | h) = \prod_{i = 1}^{|r|} \textbf{p}(r_i | h) =  \begin{cases}
q(r_i) & r_i = h_{p(r_i)} \\
1 - q(r_i) & r_i \neq h_{p(r_i)} \\
\end{cases}
\label{rm:bq}
\end{equation}
\noindent
where $q(r_i)$ is a probability of the base $r_i$ being sequenced correctly and $p(r_i)$ is position of the base. 


\section{Haplotype inference}
\label{sec:hap_inference}
It turns out that each multiplier in the haplotype model (\ref{hm:model}) can be computed in $\mathcal{O}(1)$ time. For the recombination model introduced earlier, the complexity is straightforward. Using the PBWT algorithm \cite{pbwt}, we efficiently represent $H$ as a structure which supports lookup of  the fraction value (\ref{hm:frac}) in constant time. Therefore, it remains to compute the exponential sum over all possible recombination breakpoints and over possible diplotypes. We approximate the sum by the following Hidden Markov model (HMM). 

\subsection{Hidden Markov model}
\label{HMM_inference}
We denote a state of HMM as $S$, while $S^{-1}$ denotes an antecedent state of $S$. A step further is always made upon a single marker $p$, i.e. $h^\alpha(S) = h^\alpha(S^{-1}) + h^\alpha_{p}$, where  $h^\alpha_{p} \in \{0,1\}$. Here \textit{zero} denotes consensus with the reference, while \textit{one} denotes a consensus with the alternative allele (see Sec. \ref{intro:phasing_problem} for details). Thus, a state $S$ represents

\begin{itemize}
	\item a position $p \in <1,N>$ along the chromosome. 
	\item $e^{\alpha}$ and $e^{\beta}$, denoting positions where the latest recombination event occurs for maternal and paternal haplotype respectively, thus $e^{\alpha} \in <1,p>$ as well as $e^{\beta} \in <1,p>$. 
	\item $h^{\alpha} $ and $h^{\beta}$, denoting maternal and paternal haplotype sequences up to the current position $p$, i.e. the sequences begin in the first position and continue up the current position $p$.  
	
\end{itemize}

\subsection{State space exploration}
\label{HMM:merging}
We move from left to right along the chromosome. Being in position $p$, we consider both possible diplotypes, i.e.  $h_{p+1}^{\alpha}=1$, while $h_{p+1}^{\beta}=0$ and $h_{p+1}^{\alpha}=0$, while $h_{p+1}^{\beta}=1$ for each heterozygous variant. For homozygous variants, we consider a single case where haplotypes are equal. We also consider the possibility of a recombination in $p$ on maternal, paternal, or both haplotypes. \\
Probability of the state $S$ given $S^{-1}$ is computed as follows
\begin{equation}
\begin{gathered}
\textbf{p}(S| H, R, S^{-1}) = 
\textbf{p}(S|H, S^{-1}) \textbf{p}(S|R, S^{-1}), \\
\end{gathered} \label{hmm:step}
\end{equation}
which follows from (\ref{dm:model}). Then, 
$\textbf{p}(S|H, S^{-1})$ is computed as
\begin{equation}
\begin{gathered}
\textbf{p}(S|H, S^{-1}) = \frac{\textbf{p}(S, H, S^{-1})}{\textbf{p}(H, S^{-1})} = 
\frac{\textbf{p}(S, S^{-1}| H)\textbf{p}(H)}{\textbf{p}(S^{-1}|H)\textbf{p}(H)} =  \frac{\textbf{p}(S, S^{-1}| H)}{\textbf{p}(S^{-1}|H)}
\end{gathered} \label{hmm:hm_step}
\end{equation}
The second part of the (\ref{hmm:step}), $\textbf{p}(S|R, S^{-1})$, is computed as 
\begin{equation}
\begin{gathered}
\textbf{p}(S|R, S^{-1}) = \frac{\textbf{p}(S, S^{-1}, R)}{\textbf{p}(S^{-1}, R)} = 
\frac{\textbf{p}(S, S^{-1}| R)\textbf{p}(R)}{\textbf{p}(S^{-1}|R)\textbf{p}(R)} = 
\frac{\textbf{p}(S, S^{-1}| R)}{\textbf{p}(S^{-1}|R)}
\label{HMM:rm_step}
\end{gathered}
\end{equation}

Further, we decompose $R$ into $R_{:p(S^{-1})}$, $R_{p(S):}$ and $R_{:p(S):}$ i.e. reads, which end in positions $p(S^{-1})$, which start at position $p(S)$ and which span both respectively. Reads which do not belong to any of the sets do not influence (\ref{rm:read_set}) according to (\ref{rm:bq}). This allows as to rewrite the equation above as

\begin{equation}
\begin{gathered}
\frac{\textbf{p}(S, S^{-1}| R)}{\textbf{p}(S^{-1}|R)} \propto \frac{\textbf{p}(R|S, S^{-1})}{\textbf{p}(R| S^{-1})} = \\
\frac{\textbf{p}(R_{:p(S^{-1})}, R_{p(S):},R_{:p(S):}|S, S^{-1})}{\textbf{p}(R_{:p(S^{-1})}, R_{p(S):},R_{:p(S):}|S^{-1})} = \\
\frac{\textbf{p}(R_{:p(S):}|S, S^{-1}) \textbf{p}(R_{:p(S^{-1})}|S^{-1}) \textbf{p}(R_{p(S):}| S)}{\textbf{p}(R_{:p(S^{-1})}|S^{-1})\textbf{p}(R_{:p(S):}| S^{-1})} = \\
\frac{\textbf{p}(R_{:p(S):}| S, S^{-1}) \textbf{p}(R_{p(S):}|S)}{\textbf{p}(R_{:p(S):}| S^{-1})}
\label{HMM:rm_step2}
\end{gathered}
\end{equation}
\\ \\
In case of a recombination for state $S$ on haplotype $h$ the so called\textit{ states merging} occur. The step is necessary to preserve beam diversities, as it was already mentioned in Sec. \ref{eagle:step2}. In this case we merge states which correspond to the previous position and which agree in a considered haplotype $h$ from the previous recombination point $e$ to the current position $p(S)$. More formally, states from the state set  $\mathcal{S}^M = \{ S^m | p(S^m) = p(S^{-1})$ \textit{and} $\forall S^m_1, S_2^m \in \mathcal{S}^M: h_{e:}(S_1^m) = h_{e:}(S_2^m)\}$ are merged into a single state which we refer to as $S'$. \\
\tab Therefore, the likelihood of $S'$ becomes conditioned on likelihoods of the states from $\mathcal{S}^M$. We approximate the probability as follows
\begin{equation}
\textbf{p}(S'| H, R, S^M) = \sum_{S^m \in \mathcal{S}^M}^{} \textbf{p}(S^m| H, R)
\label{HMM:states_merging}
\end{equation}
In the case of states merging, we also approximate the posteriori $\textbf{p}(r | h(S'))$ for haplotype $h$ by the weighted sum of the read $r$ posteriors over states from $\mathcal{S}^M$ as follows
\begin{equation}
\textbf{p}(r | h(S')) = \frac{\sum_{S^m \in \mathcal{S}^M}^{} \textbf{p}(r | h(S^m))\textbf{p}(S^m | H,R) }{\sum_{S^m \in \mathcal{S}^M}^{} \textbf{p}(S^m | H,R)} 
\label{HMM:read_merging}
\end{equation}



\chapter{Implementation}
\label{ch:impl}
While the high-level implementation of the proposed method is similar to the Eagle2, the algorithms differ in several ways. First of all, we use a different representation of known haplotypes from \textit{H} we refer to as \textit{PBWT Index}. Next, we incorporate probabilities of reads from $R$ into the model. A diplotype inference under the proposed HMM remains the same for both algorithms. \\
\tab Therefore, the proposed algorithm could be divided into three steps  
\begin{itemize}
	\item \textbf{Step 1}: Construct a read panel.
	\item \textbf{Step 2}: Build \textit{Index} using the PBWT algorithm.
	\item \textbf{Step 3}: Infer the most likely diplotype under the model introduced in Sec. \ref{HMM}. 
\end{itemize}

We had implemented a new bioinformatics tool which creates a read panel structure in a desired format. Since the structure might be efficiently utilized for other application, the \textit{step 1} is implemented separately from the entire phasing algorithm. \\
\tab In this chapter we first describe implementation details of the algorithm performing a read panel construction. Then we describe implementation of the hybrid phasing algorithm and discuss those algorithms' time complexity. Also, we list the algorithms' requirements, input arguments, and present examples of execution commands in the chapter. 

\section{Tool for Read Panel Construction}
\label{read_tool}

 The implemented tool for read panel creation produces a \textit{.var} file, given \textit{.vcf} and \textit{.bam} files. The newly proposed file \textit{.var} format is designed in a way allowing to iterate over variants listed in a \textit{.vcf} while preserving information about consecutive variants being spanned by a single read. \\
\tab Briefly, the tool main cycle iterates over reads in the given sorted $.bam$ file and as soon as a read covers a variant from the given \textit{.vcf}, it accesses the read alignment to the variant and calculates the aligned bases probability for each allele of the variant according to (\ref{rm:bq}). While iterating over the read set, the algorithm simultaneously moves along the sorted variant set. To reduce the memory overhead, results are frequently stored into a file during the algorithm runtime. \\
\tab The algorithm is capable of processing all types of reads (including paired-end reads produced by the Illimina platform) of any length and base qualities. The algorithm time complexity is  $\mathcal{O}(|R|*v_{max})$, where $|R|$ is number of aligned reads and $v_{max}$ is the maximum number of variants a read from $R$ spans. \\


\subsection{The .var format}
\label{sec:var}


\begin{figure}[h]
	\includegraphics[scale=0.6]{imgs/var}
	\centering
	\caption{Example of the \textit{.var} format.}
	\label{var_format}
\end{figure}

The \textit{.var} file, produced by the tool, contains the posteriori of reads given variant alleles as well as reads' base qualities calculated according to (\ref{rm:bq}).
Example of a file in \textit{.var} format is presented in Fig. \ref{var_format}. Each line of the file contains information about a single read and is composed by the read name, the list of covered markers' start positions separated by semicolons, and the list of the variants posteriori separated by semicolons. Each variant posteriori is in form of a list where each element is a comma separated list of posteriori of the variant alleles. \\
\tab The variant posteriori probability is presented in a format common for base quality representation, known as \textit{Phred} or \textit{Q score}. A Q score value is an integer value representing the estimated probability of an error, i.e. that the base is incorrect. The representation helps to avoid manipulations with floating point numbers. If \textit{p} is the error probability and \textit{q} is a Q score, then conversion between the formats is performed as:
\begin{equation}
\begin{gathered}
p = 10^{-\frac{q}{10}} \\
q = -10log_{10}(p) 
\end{gathered}
\end{equation}

The variant alleles posteriori probability are computed from sequencing qualities of the aligned read bases, which are available in a $.bam$ file in the Phread format. Therefore, the probability for a read can be computed as long as the read cigar string (see Sec. \ref{sec:data_and_formats}), containing information about the read alignment, is available in the \textit{.bam} file. 



\subsection{Execution}

\subsubsection*{Dependencies}

The tool requires \textit{Python 3.x} to be installed. Also, it requires \textit{numpy}, \textit{pysam} and \textit{biopython} packages to be installed.
\subsubsection*{Arguments}
\noindent
The script arguments are

\begin{itemize}
	\item Positional arguments
		\begin{verbatim}
			bam_path - absolute path to the .bam file 
			vcf_path - absolute path to the .vcf file
			out_prefix - prefix of the output .var file, including file location
			sample_id - sample id, identical to the id in the .vcf file 
		\end{verbatim} 
	\item Optional arguments
	\begin{verbatim}
		--config - configuration/reference id specified in the .vcf file
		--varmax - the position at which variant processing will end [All] 
		--homo - include variants in which the sample is homozygous [False]
		--indels - include INDELs [True]
		--v - verbose mode [False]
	\end{verbatim}
	
\end{itemize}

Note that \verb --config  argument is required to be specified in the case when the given $.vcf$ file contains data for multiple reference sequences. The program will set the reference id automatically otherwise. 
 
\subsubsection*{Examples}
\noindent
\textbf{Example 1}: To create \verb hg00096_read_panel.var  file for a sample \verb HG00096  which would contain all SNPs specified in \verb variants.vcf  (containing data for a single configuration) in which the sample is heterozygous for reads in \verb reads.bam , run 

\begin{verbatim}
     python3 createVar.py reads.bam
                          variants.vcf
                          hg00096_read_panel
                          HG00096
\end{verbatim}

\noindent
\textbf{Example 2}: To create \verb hg00096_read_panel.var   file for a sample \verb HG00096  which would contain substitutions listed in \verb variants.vcf   (containing data for a single configuration) including variants in which the sample is homozygous, for reads in \verb reads.bam   run 

\begin{verbatim}
     python3 createVar.py reads.bam
                          variants.vcf
                          hg00096_read_panel
                          HG00096
                          --homo=True
                          --indels=False
\end{verbatim}


\section{Phasing Algorithm}

\subsection{Step 1: Read panel construction}
As the first step, we apply the tool, presented in the previous section, to access reads posteriori probabilities. We call \verb createVar.py  for the given \textit{.bam} and \textit{.vcf} containing the sample variants which we want to phase, for heterozygous variants only, for both substitutions and INDELs.  We exclude homozygous variants since for phasing under the read panel this variants are invaluable. The phasing algorithm then takes the created \textit{.var} as an input.


\subsection{Step 2: PBWT Index construction}

Next, we build the \textit{Index} structure using the PBWT algorithm \cite{pbwt}. The algorithm takes $H$, containing known haplotypes, and iteratively creates sorted lists of haplotype prefixes, moving the prefix start point from right to left. Therefore, at each position $p$ we poses information about how many individuals have $zero$ at positions $p+1$ and how many have $one$. The information is crucial for inference under the haplotype model (\ref{haplotype_model}).\\
\tab The index construction takes $\mathcal{O}(N*K)$ time where $K$ is the number of individuals in reference panel and $N$ is total number of markers.

\subsection{Step 3: Haplotype inference}

After both \textit{Index} and read panel structures are constructed, the inference procedure starts. To find the most likely sequence of hidden states, we iterate over the solution space consecutively moving from marker to marker extending each state by \textit{eight} possible scenarios in each heterozygous variant and by \textit{four} scenarios in each other variant. \\
\tab The branching factors are given by the fact that we consider $four$ possible recombination scenarios, which are no recombination, recombinations on maternal or paternal haplotype only, or recombination on both haplotypes and possible extentions of the haplotype. Being in position $p$, we consider both possible diplotypes, i.e.  $h_{p+1}^{\alpha}=1$, while $h_{p+1}^{\beta}=0$ and $h_{p+1}^{\alpha}=0$, while $h_{p+1}^{\beta}=1$ for each heterozygous variant and we consider a single case where haplotypes are equal for each homozygous variant. We compute the likelihood of the extended state according to model described in Sec. \ref{HMM_inference}.\\
\tab Once we have crossovered states, we merge those with the same history (haplotype till the last recombination breakpoint) into a single state, as it is formalized in Sec. \ref{HMM:merging}. The likelihood of a state produced by the merging operation is then computed by (\ref{HMM:states_merging}), while posteriori of each read is approximated according to (\ref{HMM:read_merging}). The merging is performed in such a way that states are first sorted according to the $64$ bit hashcode of the state history, after which states with the same hashcodes are merged. After the merging, we extend each state by possible combination(s) of alleles, as described above.\\
 \tab After that, we prune the set by selecting only the $P$ most likely states in order to avoid the exponential growth of the solution space. Therefore, we have a maximum of $P$ states in each step of the proposed HMM. Pseudocode of the complete inference procedure is formalized in Alg. \ref{pseudocode}. \\
 \tab After this step, we obtain $h^{\alpha}_{best}$, $h^{\beta}_{best}$ - the most likely haplotypes. The algorithm then writes all variants in the original $.vcf$ into a new phased $.vcf$.
\newpage
\begin{algorithm}[H]
	\KwData{$H$ - Index, $R$ - Read Panel, $g$ - sample genotype, $P$}
	\KwResult{$h^{\alpha}_{best}$, $h^{\beta}_{best}$} $\newline$
	$m = 1 \newline$ 
	$\mathcal{S} = \{State(h^{\alpha} = "", e^{\alpha} = 0, \hspace*{59mm} // $ init state space $\newline$ 
	$\hspace*{19mm} $ $h^{\beta} = "", e^{\beta} = 0)\}$ $\newline$ 
	\While{m < N}{
	$\mathcal{S}.update\_references(H) \hspace*{37mm}//$  move along Index for $\forall s \in S$ \\
	$\mathcal{S}_m =  \mathcal{S}$ \\
	$\mathcal{S}_m.crossover()$ $ \hspace*{66mm}// $ crossover each $s$ in $S_m$ \\
	$\mathcal{S}_m.merge()$ $\tab \hspace*{43mm} // $ merge states with the same history \\
	\uIf{$g_m^1 = g_m^2$}{
		$\mathcal{S} = \mathcal{S}_m.extend(g_m^1, g_m^2) \hspace*{4mm}//$ extend both $h^{\alpha}$ and $h^{\beta}$ by $g_m^1 = g_m^2 $ for $\forall s \in S$
	}
	\Else{
		$\mathcal{S} = \mathcal{S}_m.extend(g_m^1,g_m^2) \hspace*{1mm} \cup  \hspace*{7mm}$ // extend $h^{\alpha}$ by $g_m^1$ and $h^{\beta}$ by $g_m^2$ for $\forall s \in S$\\
		$\hspace*{7mm}  \mathcal{S}_m.extend(g_m^2,g_m^1) \hspace*{11mm}$ // extend $h^{\alpha}$ by $g_m^1$ and $h^{\beta}$ by $g_m^2$ for $\forall s \in S$\\
	}
	$\mathcal{S}.compute\_likelihoods()$ \\
	$\mathcal{S} = \mathcal{S}.get\_best\_states(P) \hspace*{34mm}$ // get $P$ the most likely states \\
	$m$ += 1
	} 
	$S_{best} = \mathcal{S}.get\_best\_state() \hspace*{27mm} // $ get state with the highest likelihood \\
	\Return{$h^{\alpha}(S_{best} )$, $h^{\beta}(S_{best} )$} $\hspace*{41mm} // $ return estimated diplotype $\newline$  $\newline$ 
	\caption{High level implementation of the hybrid phasing algorithm.}
	\label{pseudocode}
\end{algorithm}

\section{Computational cost}
\label{sec:com_cost}
In this section we discuss complexity of each step of the algorithm described in the previous section (see. Alg. \ref{pseudocode}). The algorithm time complexity is primarily determined by the number of variants $N$, over which the algorithm iterates. \\
\tab Crossover of a state (notated as \textit{crossover()} in Alg.\ref{pseudocode}) is considered at each variant. It results in four possible scenarios, which are \textit{(1)} no recombination, \textit{(2)} recombination on paternal haplotype,\textit{ (3)} recombination of paternal haplotype or\textit{ (4)} both. When we consider crossover for a haplotype, we literally forget the state history and rewrite the last crossover point for the haplotype. This operation has near constant time and is performed for each of the $P$ states obtained in the previous step of HMM. Hence, it takes $\mathcal{O}(P)$ time. Evidently, the step extends the state space by a factor of \textit{four}. Since we consider only the most likely $P$ states at each step, the crossover operation results in $4P$ states. \\
\tab Then we perform states merging (notated as\textit{ merge()} in Alg. \ref{pseudocode}), which requires first to sort all haplotypes of the $4P$ states. The sorting, performed on maternal and on paternal haplotypes histories, requires $\mathcal{O}(2*4P*log4P) = \mathcal{O}(P*logP)$. In case of merging, we also perform summation of the merged states' likelihoods together with reads posteriori averaging. This takes $\mathcal{O}(4*P + 4*P*C_{max}) = \mathcal{O}(P*C_{max})$ in total. \\
\tab It has to be mentioned that states merging reduces the number of states $4P$, obtained during the crossover step.We discuss the reduction in the next section. Nevertheless, we assume that the number of states is $\mathcal{O}(4P)$, as was inferred above, for simplicity .  \\
\tab Next, we extend each state according to \textit{one} (homozygous marker) or \textit{two} (heterozygous marker) possible scenarios, as was described in the previous section. Thus, we result in $\mathcal{O}(8P)$ states after the step. For each such state we compute its likelihood under model described in Sec. \ref{HMM:merging}. The first term in (\ref{dm:model}) requires $\mathcal{O}(1)$, due to the fact that querying haplotype frequencies under \textit{PBWT Index} structure takes near constant time. Computing of (\ref{HMM:rm_step}) value requires to iterate over all reads that span the position, which takes $\mathcal{O}(C_{max})$ time. Thus, the step requires $\mathcal{O}(8*{P}*C_{max})$ in total.  \\
\tab Next, we select \textit{P} the best states according to their likelihoods at each step in $\mathcal{O}(8*P*log8P)= \mathcal{O}(P*logP)$ time, due to sorting of the states by their likelihoods. Those selected states are then propagated to the next iteration of the HMM.\\
\tab Overall, the phasing algorithm computational complexity is $\mathcal{O}(N*P(LogP + C_{max}))$.



\subsection{Discussion}

It was shown in the previous section that the overall complexity of the algorithm is linearly depend on number the of markers $N$ as well as on the number of HMM states $P$ we remember in each iteration. Regarding the remaining summation, we could not say for sure which of $P$ and $C_{max}$ would be dominated. Those values depends on user preferences and on input data. In general settings we assume that $P \in <100,1000>$ and $C_{max} \in <5,30>$. However, it does not hold universally. For example, deep sequencing, which is used to identify mutations within tumors or other rare clonal types, or microbes comprising as little as $1\%$ of the original sample \cite{illumina}, might have depth ranging from $80\times$ up to $thousands$. 


\subsubsection*{Hyperparameter \textit{P}}
The only hyperparameter of the algorithm is the number of states $P$ we remember in each step of HMM. While the time requirements of the algorithm would grow exponentially with unbounded $P$, we expect that reasonable settings of $P$ will be a good approximation. We had also expected that accuracy of phasing would grow with increasing $P$, since unbounded number of states would lead to an exact solution under the proposed model. However, we had empirically estimated that the relation is not straightforward and results obtained for smaller $P$ values are comparable to those obtained with higher $P$ values. We discuss setting of the parameter in the following chapter. 

\subsubsection*{States merging}

As is was already mentioned in the previous section, the number of states in often essentially reduced after states merging operation is performed. Unfortunately,  the factor could not be exactly inferred and has to be estimated empirically. This is because results of the merging is highly influenced by data present in reference panel as well as by decisions made in the previous steps of the inference procedure.   

\section{Execution}

The hybrid phasing algorithm is implemented in \textit{Rust} programming language. To execute the program user runs \verb phase.exe   located in \verb target/release/  directory. Along with other arguments listed below, the program takes a \textit{.vcf} containing unphased genotype information and outputs a \textit{.vcf} with the phased genotype. 

\subsubsection*{Arguments}
\noindent
The program arguments are
\begin{itemize}
	\item Required arguments
	\begin{verbatim}
		-p, --panel - absolute path to the reference panel (.vcf/.bcf)
		-i, --input - absolute path to variation sample (.vcf/.bcf)
		-o, --output - prefix of the output .vcf file, including file location
		-g, --gmap - absolute path to the genetic map file
	\end{verbatim}
	
	\item Optional arguments
	\begin{verbatim}
		-r, --reads - read panel file name (.var) [None]
		-M, --markers - # markers to phase [All]
		-N, --refs - # haplotypes from the reference panel which will be used [All]
		-D, --drop-states - # HMM states [100]
	\end{verbatim}
	
\end{itemize}

Since the algorithm can phase also without reads panel, \verb --reads  argument is optional. Also, if \verb --input  argument specifies a $.vcf$ containing more than one sample, only the first one will be phased.  

\subsubsection*{Examples}

To run the program with a read panel, a $.var$ file has to be created first using the tool introduced in Sec. \ref{read_tool}. The file is then supplied to the program using \verb --reads  argument. \\

\noindent
\textbf{Example 1}: To phase \verb hg00096_unphased.vcf  into \verb hg00096_phased.vcf   in a short time, utilizing all haplotypes from the reference panel \verb reference_panel.vcf  and a read panel \verb hg00096_read_panel.var , run

\begin{verbatim}
     phase -p reference_panel.vcf
           -i hg00096_unphased.vcf
           -o hg00096_phased
           -r hg00096_read_panel.var
           -g gmap.txt
           -D 100
\end{verbatim}

This command produces a \verb hg00096_phased-best.vcf.gz , containing the most likely diplotype estimated for the sample. Note that \verb --D  parameter corresponding to number of HMM states remembered at each iteration influences the algorithm's computational time. \\

\noindent
\textbf{Example 2}: To phase the first $1000$  variants from \verb hg00096_unphased.vcf  into \verb hg00096_phased.vcf   in a short time, utilizing $500$ haplotypes from the reference panel \verb reference_panel.vcf  and a read panel \verb hg00096_read_panel.var , run

\begin{verbatim}
     phase -p reference_panel.vcf
           -i hg00096_unphased.vcf
           -o hg00096_phased
           -r hg00096_read_panel.var
           -g gmap.txt
           -D 100
           -M 1000
           -N 500
\end{verbatim}




