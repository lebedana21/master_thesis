The tool creates a *.var* file, given *.vcf* and *.bam* files.
The newly proposed file *.var*  format is designed in a way
allowing to iterate over variants listed in a *.vcf*  while preserving information
about consecutive variants being spanned by a single read.

Each line of the file contains information about a single read and is composed 
by the read name, the list of covered markers' start positions separated by semicolons, 
and the list of the variants posteriori separated by semicolons. Each variant posteriori is in form of a 
list where each element is a comma separated list of posteriori of the variant alleles.

.var file example:

![alt text](img/var_example.png)

Paired-end reads (reads produced by the Illimina platform) as well as single-end reads of any length and base
qualities are supported. 

Requirements 
------------

The tool requires *Python 3.x* to be installed. Also, it requires *numpy*, *pysam*
and *biopython* packages to be installed.

Arguments 
---------

Positional arguments:

* bam_path - absolute path to the .bam/.sam file
* vcf_path - absolute path to the .vcf file 
* out_prefix - prefix of the output .var file, including file location
* sample_id - sample id, identical to the id in the .vcf file

Optional arguments:

* -config - configuration id corresponding to the reference id specified in the .vcf file [is read from the .vcf]
* -varmax - the position at which variant processing will end [All]
* -homo - include variants in which the sample is homozygous [False] 
* -indels - include INDELs [True]
* -v - verbose mode [False]

Notes
-----

* The given *.bam* file has to be sorted.
* A configuration is estimated automatically when the given *.vcf* contains variants for a single configuration. Otherwise, the configuration id has to be specified with *-config* argument.
