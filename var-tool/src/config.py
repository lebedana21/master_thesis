import numpy as np

class MappingConf:
    global create_reverse_int_qualities_map
    def create_reverse_int_qualities_map(range_from, range_to, int_max):
        int_to_reverse_int_map = dict()
        for i in range(range_from, range_to):
            prob_float = int_prob_2float(i)
            int_to_reverse_int_map[i] = float_prob_2int(1 - prob_float, int_max)
        return int_to_reverse_int_map

    global float_prob_2int
    def float_prob_2int(p, int_max):
        if p == 1: return 0
        if p == 0: return int_max
        return int(-10 * np.log10(p))

    global int_prob_2float
    def int_prob_2float(intp):
        return np.power(10, -intp / 10)

    READ_INNER_FLAG = [-1]
    EPSILON_INT = 100  # equal to 1e-10
    EPSILON_FLOAT = np.power(10, -EPSILON_INT / 10)
    ERROR_BASE_MAX = 255  # real lower bound for base error is 256
    ERROR_BASE_MAX_FLOAT = np.power(10, -ERROR_BASE_MAX / 10)
    DELETION_SYMBOL = ""
    # read to allele alignment type
    DELETION_FLAG = -1
    INSERTION_FLAG = 1
    MATCH_FLAG = 0
    # i.e. combination of some
    OTHER_FLAG = -2
    REVERSE_INT_QUALITY_MAP = create_reverse_int_qualities_map(range_from = 0,
                                                               range_to = 256,
                                                               int_max = ERROR_BASE_MAX)





