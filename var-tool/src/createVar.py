from collections import deque
import pysam
import time
import readPanel, variant
from pysam import VariantFile
import argparse


def filter_by_type(allles, include_indels):
    ''' Filters variants which are INDELs by the alleles' length.
     Returns a flag determining if the variant will be filtered.'''
    for a in allles:
        if len(a) != 1 and not include_indels: return True # indel
        if "<" in a : return True # structural variant
    return False

def is_hetero_variant(variant, sample_name):
    '''Checks if the sample `sample_name` is heterozigous in the variant `variant`.'''
    sample_info = variant.samples.get(sample_name)
    if 1 in sample_info.allele_indices and 0 in sample_info.allele_indices:
        return True
    return False

def get_variants(vcf_reader, samtool_region, include_homo, include_indels, print_skiped):
    ''' Reads subset of variants (excludes structural; INDELs option is
     specified by `include_indels` and homozigous option is specified by `heter_only`)
     from the `vcf_reader` file. Returns a variant list. '''
    variant_list = deque([]); variant_pos_set = set(); repeat_count = 0
    for v in vcf_reader.fetch(region= samtool_region):
        # skip structural and indels (if specified)
        if filter_by_type(v.alleles, include_indels):
            if print_skiped: print("W: Skip variant %r: %r "
                                   % (v.pos, str(list(v.alleles))))
            continue
        # skip repeating
        if (v.pos - 1) in variant_pos_set:
            repeat_count += 1
            if print_skiped: print("W: Skip repeating variant %r: %r "
                                   % (v.pos, str(list(v.alleles))))
            continue

        # skip -homo variants
        if not include_homo and not is_hetero_variant(v, args.sample_id): continue
        # append to variant list
        allele_list = [a.lower() for a in v.alleles]
        variant_list.append(variant.Variant(pos = (v.pos -1),
                                            ref = v.ref.lower(),
                                            allele_list = allele_list))
        variant_pos_set.add(v.pos - 1)
    return list(variant_list)

if __name__ == "__main__":

    # get arguments values
    parser = argparse.ArgumentParser()
    parser.add_argument("bam_path", type=str, help=".bam containing reads")
    parser.add_argument("vcf_path", type=str, help=".vcf variants will be taking from")
    parser.add_argument("out_prefix", type=str, help="prefix of the output file")
    parser.add_argument("sample_id", type=str, help="sample id (for the navigation)")

    parser.add_argument("--config", default=None, type=str, help="configuration id (reference id) in .vcf")
    parser.add_argument("--varmax", default=None, type=int, help="position, till which variants will be proceed")
    parser.add_argument("--homo", default=False, type=lambda s: s.lower() in ['true', 't', 'yes', '1'], help="include homorozygous variants")
    parser.add_argument("--indels", default=False, type=lambda s: s.lower() in ['true', 't', 'yes', '1'], help="include INDELs")
    # parser.add_argument("-njobs", default=1, type=int, help="number of jobs (parallel executions)")
    # parser.add_argument("-job", default=1, type=int, help="current job number")
    parser.add_argument("-v", default=False, type=lambda s: s.lower() in ['true', 't', 'yes', '1'], help="verbose mode")

    args = parser.parse_args()
    print("Include homorozygous variants: %r" %(args.homo))
    print("Include INDELs: %r" %args.indels)

    # read .vcf
    stime = time.time()
    vcf_reader = VariantFile(args.vcf_path, drop_samples=False)
    # determine config id
    if args.config is None:
        config_id = vcf_reader.get_reference_name(False)
        assert type(config_id) is str,\
            "More than one configuration in .vcf file. Please, specify " \
            "one from the list %r with -config argument" % str(config_id)
    else: config_id = args.config

    # generate region to process
    idx_from = 1
    if args.varmax is not None:
        region = str(config_id) + ':1:' + str(args.varmax)
        print("Process region %r.. " % region)
    else:
        region = None

    print("--------------------------------------")
    print("Read variants from %r.. " % args.vcf_path)

    # read variants from the .vcf
    variant_list = get_variants(vcf_reader=vcf_reader,
                                samtool_region=region,
                                include_homo=args.homo,
                                include_indels=args.indels,
                                print_skiped=args.v)

    print("Done in %d sec.; Total number of variants is %r." %(time.time() - stime,
                                                          str(len(variant_list))))

    # read .bam
    stime2 = time.time()
    panel_fname = args.out_prefix + ".var"
    samfile = pysam.AlignmentFile(args.bam_path, "rb")  # open .bam file

    # determine config id
    sam_config_id = samfile.get_reference_name(False)

    # create read panel
    print("--------------------------------------")
    print("Process reads from %r.. Configuration id is %r" % (args.bam_path, sam_config_id))

    read_panel = readPanel.ReadPanel(variant_list=variant_list)
    read_panel.fill(samfile=samfile,
                    output_name=panel_fname)  # fill reads panel

    stime_end = time.time()
    print("Done in %d sec.; Total time is %d sec." % (stime_end-stime2, stime_end-stime))
    print("All reads are stored into %r" % panel_fname)



