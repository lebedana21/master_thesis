import os
import numpy as np


class OutputFile:
    '''Class to manage writing to the output .var file'''
    def __init__(self, fname):
        self.fname = fname
        self.ipos_storefrom = 0
        self.rfile = self.open()

    def open(self):
        fpath = os.path.join("", self.fname)
        if os.path.exists(fpath):
            print("W: File %r exists and will be rewritten" %fpath)
        rfile = open(fpath, 'w+')
        return rfile

    def store_read(self, read_instance):
        probs_str, varpos_str = read_instance.covered_variants_dict.to_str()
        self.rfile.write(read_instance.read.qname + " ") # id
        self.rfile.write(varpos_str + " ")  # variants pos reads cover
        self.rfile.write(probs_str) # covered variants
        self.rfile.write("\n")

    def store(self, ipos_bound, rpanel):
        while self.ipos_storefrom < ipos_bound:
            rpanel[:, self.ipos_storefrom].tofile(self.rfile, sep=" ", format="%s")
            self.rfile.write("\n")
            self.ipos_storefrom += 1
        self.ipos_storefrom = ipos_bound

    def close(self):
        self.rfile.close()



