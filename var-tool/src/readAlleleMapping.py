from Bio import pairwise2
from config import MappingConf as CONFIG

def get_read_variant_posteriori_list(read, read_aligned_pairs, variant, nskipped, iblock, nsoft_clipping):
    ''' Returns posteriori of the read for each allele of the variant'''
    # find read to ref fragments alignment
    [alignment_idx, nskipped, iblock, nsoft_clipping] = \
        find_alignment_start_idx(variant, read, read_aligned_pairs, nskipped, iblock, nsoft_clipping)
    [aligned_fragment, aligned_ref_fragment] = align_read_to_ref(idx_from = alignment_idx,
                                                                 max_allele_len = variant.max_allele_len,
                                                                 expected_ref_len = len(variant.allele_list[0]),
                                                                 read=read,
                                                                 read_aligned_pairs=read_aligned_pairs)

    posteriori_list_int = [0] * len(variant.allele_list)
    if len(aligned_ref_fragment) == len(variant.allele_list[0]):
        for i, allele in enumerate(variant.allele_list):
            if aligned_fragment != allele and len(aligned_fragment)!= len(allele):
                r_aligned_fragment, a_aligned_fragment = align_sequences(aligned_fragment, allele)
            else:
                r_aligned_fragment, a_aligned_fragment = aligned_fragment, allele
            p = compute_read_allele_posteriori(r_aligned_fragment, a_aligned_fragment, alignment_idx, read, read_aligned_pairs, nsoft_clipping, i)
            posteriori_list_int[i] = p
    # store obtained alleles posteriori for the aligned read's fragment
    variant.add_read_alignment_allele_posteriori_list(aligned_fragment, posteriori_list_int)
    return posteriori_list_int, nskipped, iblock, nsoft_clipping

def align_read_to_ref(idx_from, max_allele_len, expected_ref_len, read, read_aligned_pairs):
    ''' Extract read alignment to reference sequence '''
    read_alignment = ""; ref_alignment = ""
    for i in range(0, max_allele_len):
        # if variant is only partially covered by read
        if idx_from + i >= len(read_aligned_pairs): break
        aligned_pair = read_aligned_pairs[idx_from + i]
        read_alignment += CONFIG.DELETION_SYMBOL if aligned_pair[0] is None else read.query_sequence[aligned_pair[0]]
        ref_alignment  += CONFIG.DELETION_SYMBOL if aligned_pair[2] is None else aligned_pair[2]
        # stop if ref_alignment match expected reference allele len
        if len(ref_alignment) > expected_ref_len:
            return read_alignment[:-1].lower(), ref_alignment[:-1].lower()
    return read_alignment.lower(), ref_alignment.lower()

def compute_read_allele_posteriori(read_alignment, allele, alignment_idx, read, read_aligned_pairs, nsoft_clipping, idx):
    '''Iterates over aligned bases and seeks a posteriori probability for each base in separate basing on
    aligned nucleotides equality and the read base sequencing quality. Note: doesn't use alignment quality so far.'''
    read_base_error_list = read.query_qualities
    posteriori = 0 #in log scale
    for i in range(len(read_alignment)):
        base_read = read_alignment[i]
        base_allele = allele[i]
        if alignment_idx + i >= len(read_aligned_pairs): break
        [base_idx, _, _] = read_aligned_pairs[alignment_idx + i]
        read_base_error = CONFIG.EPSILON_INT if base_idx is None else read_base_error_list[base_idx]
        posteriori += get_base_match_probability_int(base_read, base_allele, read_base_error)
        if posteriori > CONFIG.ERROR_BASE_MAX: return CONFIG.ERROR_BASE_MAX
    return posteriori

def get_base_match_probability_int(read_base, allele_base, read_base_quality_int):
    if read_base == allele_base:
        return CONFIG.REVERSE_INT_QUALITY_MAP[read_base_quality_int]
    else:
        return read_base_quality_int

def find_alignment_start_idx(variant, read, read_aligned_pairs, nskipped, iblock, nsoft_clipping):
    '''Iterates over cigar tuples till required position (start position of the variant) is reached.'''
    # ids indicating reference skipping
    ref_skip_type_set = set([1, 4])
    # how many ref bases were skipped
    softclipping_type = 4; nskip = nskipped;
    # get start index (no ref. skipping)
    alignment_index = variant.pos - read.pos
    cigar_tuples = read.cigartuples
    # accumulate skipped reference bases
    for i in range(iblock, len(cigar_tuples)):
        # cigar_code
        c = cigar_tuples[i]
        block_type = c[0]
        block_size = c[1]
        if block_type in ref_skip_type_set: nskip += block_size;
        if block_type == softclipping_type: nsoft_clipping += block_size;
        # check by number of skipped and theoretical index
        match_info = read_aligned_pairs[alignment_index + nskip]
        if match_info[1] == variant.pos:
            # if block changed pointer go to the next
            if block_type in ref_skip_type_set: i += 1
            return [alignment_index + nskip, nskip, i, nsoft_clipping]
    assert False, "Reference alignment for position %r was not found" % variant.pos

def align_sequences(seq1, seq2, match_score = 1, mismatch_score = -1, gap_opening = -2, gap_extention = -1.1):
    '''Performs pairwise sequence alignment'''
    alignment_list = pairwise2.align.globalms(seq1,
                                              seq2,
                                              match_score,
                                              mismatch_score,
                                              gap_opening,
                                              gap_extention)
    if len(alignment_list) == 0:
        if seq1 == '':
            return CONFIG.DELETION_SYMBOL * len(seq2), seq2
        else:
            assert False, "No alignment was found for sequences: %r and %r" %(seq1, seq2)
    (seq_aligned1, seq_aligned2, _, _, _) = alignment_list[-1]  # alignment having a start symbol aligned
    return seq_aligned1, seq_aligned2
