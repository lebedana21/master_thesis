from config import MappingConf as CONFIG
from readAlleleMapping import get_read_variant_posteriori_list

class _CoveredVariantDict:
    def __init__(self):
        self.variant_posteriori_map = dict()
        self.covered_variants_list = []

    def add_variant_posteriori(self, variant_pos, posteriori):
        if variant_pos in self.variant_posteriori_map:
            # multiply mate's prob. by newly obtained prob. for the position
            mate_prob_list = self.variant_posteriori_map[variant_pos]
            for i, p in enumerate(mate_prob_list):
                # plus since we are working in log scale
                posteriori_sumed = p + posteriori[i]
                # store the value if it doesnt not exceed the maximum values
                mate_prob_list[i] = posteriori_sumed if posteriori_sumed < CONFIG.ERROR_BASE_MAX\
                                                     else CONFIG.ERROR_BASE_MAX
        else:
            # or store posteriori and var. position
            self.covered_variants_list.append(variant_pos)
            self.variant_posteriori_map[variant_pos] = posteriori

    def is_empty(self):
        return not self.covered_variants_list

    def get_nvariant_covered(self):
        return len(self.covered_variants_list)

    def get_last(self):
        key_last = self.covered_variants_list[-1]
        return self.variant_posteriori_map[key_last]

    def remove_last(self):
        key_last = self.covered_variants_list.pop()
        self.variant_posteriori_map.pop(key_last)

    def to_str(self):
        probs_as_str = ''; varpos_as_str = ''
        for k in self.covered_variants_list:
            p_list = self.variant_posteriori_map[k]
            # plus one to correspond position in .vcf
            varpos_as_str += ":" + str(k + 1)
            probs_as_str += ":" + ','.join(str(p) for p in p_list)
        return probs_as_str[1:], varpos_as_str[1:]

class Read:
    def __init__(self, pos_from, read):
        self.pos_from = pos_from
        self.covered_variants_dict = _CoveredVariantDict()
        self.terminated = False
        self.read = read

    def add_variant_posteriori_list(self, variant_pos, posteriori_list):
        self.covered_variants_dict.add_variant_posteriori(variant_pos, posteriori_list)

    def terminate(self):
        self.remove_open_tail()
        self.length = self.covered_variants_dict.get_nvariant_covered()
        self.terminated = True

    def add_covering_varaints_posteriori_list(self, ivariant, variant_list):
        ''' Compute and add variant posteriori to the read '''
        nskipped = 0; iblock = 0; nsoft_clipping = 0
        read_aligned_pairs = self.read.get_aligned_pairs(with_seq=True)
        while ivariant < len(variant_list) and self.read.aend > variant_list[ivariant].pos:
            [p_list, nskipped, iblock, nsoft_clipping] = \
                get_read_variant_posteriori_list(self.read, read_aligned_pairs, variant_list[ivariant],
                                                                      nskipped, iblock, nsoft_clipping)
            self.add_variant_posteriori_list(variant_list[ivariant].pos, p_list.copy())
            ivariant = ivariant + 1
        return ivariant - 1

    def remove_open_tail(self):
        while self.covered_variants_dict.get_last() == CONFIG.READ_INNER_FLAG:
            self.covered_variants_dict.remove_last()

    def mark_read_inner(self, ivar_current, variant_list):
        ''' Fill up reads inner part: from the end of
            the first read to the start of the second '''
        while ivar_current < len(variant_list) and self.read.mpos > variant_list[ivar_current].pos:
            self.add_variant_posteriori_list(variant_list[ivar_current].pos, CONFIG.READ_INNER_FLAG)
            ivar_current += 1
