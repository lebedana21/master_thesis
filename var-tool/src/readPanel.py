import fileManager
from collections import deque
from readInstance import Read
import numpy as np

# defines how often processed reads will be stored into a file
EXPECTED_READ_LEN = 100

class _ReadPanelIterator:
    def __init__(self, variant_list):
        self.ivariant = 0
        self.variant_list = variant_list
        self.row_max = [0] * len(variant_list)
        self.proper_covered = [0] * len(variant_list)

    def next(self):
        self.ivariant += 1
        return not self.is_last()

    def mark_covered(self, ifrom, ito):
        for i in range(ifrom, ito+1):
            self.proper_covered[i] += 1

    def get_n_covered(self):
        return len(np.where(np.asarray(self.proper_covered) != 0)[0])

    def is_last(self):
        return len(self.variant_list) <= self.ivariant

    def is_first(self):
        return self.ivariant == 0

    def get_variant_position(self):
        return self.variant_list[self.ivariant].pos

    def get_col(self):
        return self.ivariant

    def update_pos_depth(self, row, col):
        self.row_max[col] = max(row+1, self.row_max[col])

    def zero_variant_coverage(self):
        return self.row_max[self.ivariant] == 0


class ReadPanel:
    def __init__(self, variant_list):
        self.variant_list = variant_list
        self.iterator = _ReadPanelIterator(variant_list)
        self.reads_to_unload = deque([])

    def fill(self, samfile, output_name):
        self.rfile = fileManager.OutputFile(output_name)
        self.read_from_sam(samfile)
        self.rfile.close()

    def teminate_all(self, read_expecting_mate_dict):
        '''Terminates all reads from the waiting dict.'''
        for read_name, read in read_expecting_mate_dict.items():
            read.terminate()

    def read_from_sam(self, samfile):
        ''' Iterates over all reads in the .bam/.sam file (assumes sorted) and
         computes likelihood of its alignment to the covered variants' alleles. '''
        read_expecting_mate_dict = dict() # read_name: row
        nreads = 0
        for read in samfile.fetch():
            nreads += 1
            if read.is_unmapped: continue
            # scroll to position which is after the read beginning
            if not self.to_next_variant(read):
                # end reading
                self.terminate(read_expecting_mate_dict)
                return
            # process the read
            else:
                read_instance = Read(self.iterator.get_variant_position(), read)
                self.reads_to_unload.append(read_instance)
                self.acquire_covered_variants(read_instance, read_expecting_mate_dict)
                # store terminated reads to a file
                if self.iterator.ivariant % EXPECTED_READ_LEN == 0 and not self.iterator.is_first():
                    self.unload_reads()
        # terminate all reads
        self.terminate(read_expecting_mate_dict)

    def terminate(self, read_expecting_mate_dict):
        self.teminate_all(read_expecting_mate_dict)
        self.unload_reads()  # unload all

    def unload_reads(self):
        """ Unloads terminated reads into the output file """
        read_to_unload_new = deque([])
        for r in self.reads_to_unload:
            if r.terminated:
                # store read into a file
                self.rfile.store_read(r)
            else:
                if not r.covered_variants_dict.is_empty():
                    read_to_unload_new.append(r)
        # rewrite the queue
        self.reads_to_unload = read_to_unload_new.copy()

    def to_next_variant(self, read):
        ''' Scrolls to the first variant which the read covers '''
        while read.pos > self.iterator.get_variant_position():
            self.log()
            # to empty some memory
            self.variant_list[self.iterator.ivariant] = False
            # terminate
            if not self.iterator.next(): return False
        return True

    def acquire_covered_variants(self, read_instance, read_expecting_mate):
        ''' Processes read's alignment '''
        cover = read_instance.read.aend > self.iterator.get_variant_position()
        first = read_instance.read.pos < read_instance.read.mpos and read_instance.read.is_proper_pair
        mate_in_panel = read_instance.read.qname in read_expecting_mate.keys()
        # -- read doesnt cover any position --
        if not cover:
            if not first and mate_in_panel:
                read_instance_mate = read_expecting_mate[read_instance.read.qname]
                read_instance_mate.terminate()
                del read_expecting_mate[read_instance.read.qname]
        # -- read covers the position --
        else:
            if first:
                ivariant_last = read_instance.add_covering_varaints_posteriori_list(self.iterator.ivariant, self.variant_list)
                read_instance.mark_read_inner(ivariant_last + 1, self.variant_list)
                read_expecting_mate[read_instance.read.qname] = read_instance
            else:
                if mate_in_panel:
                    # substitute current read information
                    read_instance_mate = read_expecting_mate[read_instance.read.qname]
                    read_instance_mate.read = read_instance.read
                    read_instance = read_instance_mate
                    del read_expecting_mate[read_instance.read.qname]
                read_instance.add_covering_varaints_posteriori_list(self.iterator.ivariant, self.variant_list)
                read_instance.terminate()

    def log(self, period = 2500):
        if self.iterator.ivariant % period == 0: print("# variants processed %r" %self.iterator.ivariant)

