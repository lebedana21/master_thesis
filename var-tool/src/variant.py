
class Variant:
    ''' Class represents a variant from .vcf file'''
    def __init__(self, pos, ref, allele_list):
        self.pos = pos
        self.ref = ref.lower()
        # alleles including reference allele
        self.allele_list = allele_list.copy()
        self.read_alignments_alelles_posteriori = dict()
        self.max_allele_len = self._compute_max_allele_len()

    def _compute_max_allele_len(self):
        return max([len(a) for a in self.allele_list])

    def to_string(self):
        return str(self.pos) + ": " + str(self.ref) + " " + str(self.allele_list)

    def add_read_alignment_allele_posteriori_list(self, read_alignemnt, allele_posteriori_list):
        self.read_alignments_alelles_posteriori[read_alignemnt] = allele_posteriori_list

    def get_read_alignment_allele_posteriori_list(self, read_alignemnt):
        if read_alignemnt in self.read_alignments_alelles_posteriori.keys():
            return self.read_alignments_alelles_posteriori[read_alignemnt].copy()
        return None